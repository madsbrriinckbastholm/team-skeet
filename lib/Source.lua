if game.CoreGui:FindFirstChild("Clean") then
    game.CoreGui.Clean:Destroy()
end

if game.CoreGui:FindFirstChild("SnowGui") then
    game.CoreGui.SnowGui:Destroy()
end

if game.CoreGui:FindFirstChild("mouseicon") then
    game.CoreGui.mouseicon:Destroy()
end

for i, v in next, getconnections(game:GetService("Players").LocalPlayer.Idled) do
    v:Disable()
end

local UserInputService = game:GetService("UserInputService")
local TweenService = game:GetService("TweenService")
local RunService = game:GetService("RunService")
local PlayerService = game:GetService("Players")
local LocalPlayer = PlayerService.LocalPlayer
local Mouse = LocalPlayer:GetMouse()
local http = game:GetService("HttpService")

local TeamSkeetLib = {
    Themes = {
        Default = {
            MainFrame = Color3.fromRGB(22, 22, 22),
            TopBar = Color3.fromRGB(27, 27, 27),
            TextColor =  Color3.fromRGB(255,255,255),
            TextColor2 =  Color3.fromRGB(210,210,210),
            Menu = Color3.fromRGB(37, 37, 37),
            TabToggled = Color3.fromRGB(43, 43, 43),
            Button = Color3.fromRGB(30,30,30),
            ButtonHold = Color3.fromRGB(31,31,31),
            Toggle = Color3.fromRGB(30,30,30),
            ToggleFrame = Color3.fromRGB(55,55,55),
            ToggleToggled = Color3.fromRGB(22, 168, 76),
            Slider = Color3.fromRGB(30,30,30),
            SliderBar = Color3.fromRGB(25, 25, 25),
            SliderInc = Color3.fromRGB(60, 60, 60),
            Dropdown = Color3.fromRGB(30,30,30),
            DropdownItem = Color3.fromRGB(30,30,30),
            TabTextBack = Color3.fromRGB(37-18, 37-18, 37-18),
            Circle = Color3.fromRGB(255,255,255),
            Textbox = Color3.fromRGB(30,30,30),
            TextboxFrame = Color3.fromRGB(25, 25, 25),
            -- Colorpicker = Color3.fromRGB(30,30,30),
            -- Label = Color3.fromRGB(30,30,30),
            Bind = Color3.fromRGB(30,30,30),
            BindSec = Color3.fromRGB(30-10,30-10,30-10),
        },
        Skeet = {
            MainFrame = Color3.fromRGB(25, 25, 25),
            TopBar = Color3.fromRGB(30, 30, 30),
            TextColor =  Color3.fromRGB(210, 210, 210),
            TextColor2 =  Color3.fromRGB(210,210,210),
            Menu = Color3.fromRGB(36,36,36),
            TabToggled = Color3.fromRGB(20,20,20),
            Button = Color3.fromRGB(30,30,30),
            ButtonHold = Color3.fromRGB(31,31,31),
            Toggle = Color3.fromRGB(30,30,30),
            ToggleFrame = Color3.fromRGB(55,55,55),
            ToggleToggled = Color3.fromRGB(94, 151, 42),
            Slider = Color3.fromRGB(30,30,30),
            SliderBar = Color3.fromRGB(25, 25, 25),
            SliderInc = Color3.fromRGB(94, 151, 42),
            Dropdown = Color3.fromRGB(30,30,30),
            DropdownItem = Color3.fromRGB(30,30,30),
            TabTextBack = Color3.fromRGB(45-18, 45-18, 45-18),
            Circle = Color3.fromRGB(94, 151, 42),
            Textbox = Color3.fromRGB(30,30,30),
            TextboxFrame = Color3.fromRGB(25, 25, 25),
            -- Colorpicker = Color3.fromRGB(30,30,30),
            -- Label = Color3.fromRGB(30,30,30),
            Bind = Color3.fromRGB(30,30,30),
            BindSec = Color3.fromRGB(30-10,30-10,30-10),
        },
        Discord = {
            MainFrame = Color3.fromRGB(54,57,63),
            TopBar = Color3.fromRGB(47,49,54),
            TextColor =  Color3.fromRGB(255,255,255),
            TextColor2 =  Color3.fromRGB(210,210,210),
            Menu = Color3.fromRGB(47,49,54),
            TabToggled = Color3.fromRGB(54,57,63),
            Button = Color3.fromRGB(88,101,242),
            ButtonHold = Color3.fromRGB(71,82,196),
            Toggle = Color3.fromRGB(47,49,54),
            ToggleFrame = Color3.fromRGB(67,69,74),
            ToggleToggled = Color3.fromRGB(22, 168, 76),
            Slider = Color3.fromRGB(47,49,54),
            SliderBar = Color3.fromRGB(42,44,49),
            SliderInc = Color3.fromRGB(62,64,69),
            Dropdown = Color3.fromRGB(47,49,54),
            DropdownItem = Color3.fromRGB(47,49,54),
            TabTextBack = Color3.fromRGB(47-18,49-18,54-18),
            Circle = Color3.fromRGB(255,255,255),
            Textbox = Color3.fromRGB(47,49,54),
            TextboxFrame = Color3.fromRGB(42,44,49),
            -- Colorpicker = Color3.fromRGB(47,49,54),
            -- Label = Color3.fromRGB(47,49,54),
            Bind = Color3.fromRGB(47,49,54),
            BindSec = Color3.fromRGB(47-10,49-10,54-10),
        },
        Blue = {
            MainFrame = Color3.fromRGB(35, 35, 50),
            TopBar = Color3.fromRGB(40, 40, 55),
            TextColor =  Color3.fromRGB(255,255,255),
            TextColor2 =  Color3.fromRGB(210,210,210),
            Menu = Color3.fromRGB(47, 47, 62),
            TabToggled = Color3.fromRGB(53,53,68),
            Button = Color3.fromRGB(40,40,55),
            ButtonHold = Color3.fromRGB(41,41,56),
            Toggle = Color3.fromRGB(40,40,55),
            ToggleFrame = Color3.fromRGB(65,65,80),
            ToggleToggled = Color3.fromRGB(64, 64, 120),
            Slider = Color3.fromRGB(40,40,55),
            SliderBar = Color3.fromRGB(35, 35, 50),
            SliderInc = Color3.fromRGB(70, 70, 85),
            Dropdown = Color3.fromRGB(40,40,55),
            DropdownItem = Color3.fromRGB(40,40,55),
            TabTextBack = Color3.fromRGB(47-18, 47-18, 62-18),
            Circle = Color3.fromRGB(255,255,255),
            Textbox = Color3.fromRGB(40,40,55),
            TextboxFrame = Color3.fromRGB(35, 35, 50),
            -- Colorpicker = Color3.fromRGB(40,40,55),
            -- Label = Color3.fromRGB(40,40,55),
            Bind = Color3.fromRGB(40,40,55),
            BindSec = Color3.fromRGB(40-10,40-10,55-10),
        },
        Red = {
            MainFrame = Color3.fromRGB(50, 35, 35),
            TopBar = Color3.fromRGB(55, 40, 40),
            TextColor =  Color3.fromRGB(255,255,255),
            TextColor2 =  Color3.fromRGB(210,210,210),
            Menu = Color3.fromRGB(62, 47, 47),
            TabToggled = Color3.fromRGB(68,53,53),
            Button = Color3.fromRGB(55,40,40),
            ButtonHold = Color3.fromRGB(56,41,41),
            Toggle = Color3.fromRGB(55,40,40),
            ToggleFrame = Color3.fromRGB(80,65,65),
            ToggleToggled = Color3.fromRGB(120, 64, 64),
            Slider = Color3.fromRGB(55,40,40),
            SliderBar = Color3.fromRGB(50, 35, 35),
            SliderInc = Color3.fromRGB(85, 70, 70),
            Dropdown = Color3.fromRGB(55,40,40),
            DropdownItem = Color3.fromRGB(55,40,40),
            TabTextBack = Color3.fromRGB(62-18, 47-18, 47-18),
            Circle = Color3.fromRGB(255,255,255),
            Textbox = Color3.fromRGB(55,40,40),
            TextboxFrame = Color3.fromRGB(50, 35, 35),
            -- Colorpicker = Color3.fromRGB(55,40,40),
            -- Label = Color3.fromRGB(55,40,40),
            Bind = Color3.fromRGB(55,40,40),
            BindSec = Color3.fromRGB(55-10,40-10,40-10),
        },
        Green = {
            MainFrame = Color3.fromRGB(35, 50, 35),
            TopBar = Color3.fromRGB(40, 55, 40),
            TextColor =  Color3.fromRGB(255,255,255),
            TextColor2 =  Color3.fromRGB(210,210,210),
            Menu = Color3.fromRGB(47, 62, 47),
            TabToggled = Color3.fromRGB(53,68,53),
            Button = Color3.fromRGB(40,55,40),
            ButtonHold = Color3.fromRGB(41,56,41),
            Toggle = Color3.fromRGB(40,55,40),
            ToggleFrame = Color3.fromRGB(65,80,65),
            ToggleToggled = Color3.fromRGB(64, 120, 64),
            Slider = Color3.fromRGB(40,55,40),
            SliderBar = Color3.fromRGB(35, 50, 35),
            SliderInc = Color3.fromRGB(70, 85, 70),
            Dropdown = Color3.fromRGB(40,55,40),
            DropdownItem = Color3.fromRGB(40,55,40),
            TabTextBack = Color3.fromRGB(47-18, 62-18, 47-18),
            Circle = Color3.fromRGB(255,255,255),
            Textbox = Color3.fromRGB(40,55,40),
            TextboxFrame = Color3.fromRGB(35, 50, 35),
            -- Colorpicker = Color3.fromRGB(40,55,40),
            -- Label = Color3.fromRGB(40,55,40),
            Bind = Color3.fromRGB(40,55,40),
            BindSec = Color3.fromRGB(40-10,55-10,40-10)
        }
    },
    Settings = {
        Theme = 'Skeet',
        CloseBind = "RightControl",
        User_Image = "rbxassetid://13007096788"
    },
    Flags = {},
    LP = LocalPlayer,
    Players = PlayerService,
    TweenService = game:GetService("TweenService"),
    Workspace = game:GetService("Workspace"),
    ReplicatedStorage = game:GetService("ReplicatedStorage"),
    VirtualUser = game:GetService("VirtualUser"),
    UserInputService = game:GetService("UserInputService"),
    RunS = game:GetService("RunService")
}

TeamSkeetLib.Speed = 2
TeamSkeetLib.Trans = 1

local function MakeDraggable(topbarobject,object) local Dragging=nil local DragInput=nil local DragStart=nil local StartPosition=nil local function Update(input) local Delta=input.Position-DragStart local pos=UDim2.new( StartPosition.X.Scale,StartPosition.X.Offset+Delta.X,StartPosition.Y.Scale,StartPosition.Y.Offset+Delta.Y ) local Tween=TweenService:Create(object,TweenInfo.new(0.1),{Position=pos}) Tween:Play() end topbarobject.InputBegan:Connect( function(input) if input.UserInputType==Enum.UserInputType.MouseButton1 or input.UserInputType==Enum.UserInputType.Touch then Dragging=true DragStart=input.Position StartPosition=object.Position input.Changed:Connect( function() if input.UserInputState==Enum.UserInputState.End then Dragging=false end end) end end) topbarobject.InputChanged:Connect( function(input) if input.UserInputType==Enum.UserInputType.MouseMovement or input.UserInputType==Enum.UserInputType.Touch then DragInput=input end end) UserInputService.InputChanged:Connect( function(input) if input==DragInput and Dragging then Update(input) end end) end

function Ripple(Object)
	spawn(function()
        local Circle = Instance.new("ImageLabel")
        Circle.Parent = Object 
        Circle.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Circle
        Circle.BackgroundTransparency = 1.000 
        Circle.ZIndex = 10 
        Circle.Image = "rbxassetid://266543268"
        Circle.ImageColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Circle
        Circle.ImageTransparency = 0.8 
        Circle.Position = UDim2.new(0, Mouse.X - Circle.AbsolutePosition.X, 0, Mouse.Y - Circle.AbsolutePosition.Y)
        local Size = Object.AbsoluteSize.X
        TweenService:Create(Circle, TweenInfo.new(0.35), {Position = UDim2.fromScale(math.clamp(Mouse.X - Object.AbsolutePosition.X, 0, Object.AbsoluteSize.X)/Object.AbsoluteSize.X,Object,math.clamp(Mouse.Y - Object.AbsolutePosition.Y, 0, Object.AbsoluteSize.Y)/Object.AbsoluteSize.Y) - UDim2.fromOffset(Size/2,Size/2), ImageTransparency = 1, Size = UDim2.fromOffset(Size,Size)}):Play()
        spawn(function()
            wait(0.5)
            Circle:Destroy()
        end)
        spawn(function()
            while wait() do
                Circle.ImageColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Circle
            end
		end)
    end)
end

function NotNormalTrans(object1, object2, outcome, input)
    object1.MouseEnter:Connect(function()
        TweenService:Create(
            object2,
            TweenInfo.new(.3, Enum.EasingStyle.Quad),
            {ImageTransparency = outcome}
        ):Play()
    end)
    object1.MouseLeave:Connect(function()
        TweenService:Create(
            object2,
            TweenInfo.new(.3, Enum.EasingStyle.Quad),
            {ImageTransparency = input}
        ):Play()
    end)
end

function NormalTrans(object1, outcome, input)
    object1.MouseEnter:Connect(function()
        TweenService:Create(
            object1,
            TweenInfo.new(.3, Enum.EasingStyle.Quad),
            {BackgroundTransparency = outcome}
        ):Play()
    end)
    object1.MouseLeave:Connect(function()
        TweenService:Create(
            object1,
            TweenInfo.new(.3, Enum.EasingStyle.Quad),
            {BackgroundTransparency = input}
        ):Play()
    end)
end

function MouseOver(object, object2)
    object.MouseButton1Click:Connect(function()
        TeamSkeetLib.Speed = 25
        TeamSkeetLib.Trans = 0.4
        wait(.4)
        TeamSkeetLib.Speed = 2
        TeamSkeetLib.Trans = 1
    end)
    object.MouseEnter:Connect(function()
        TeamSkeetLib.Speed = 2
        TeamSkeetLib.Trans = 0.4
        TweenService:Create(
            object2,
            TweenInfo.new(.5, Enum.EasingStyle.Quad),
            {BackgroundTransparency = TeamSkeetLib.Trans}
        ):Play()
    end)
    object.MouseLeave:Connect(function()
        TeamSkeetLib.Speed = 2
        TeamSkeetLib.Trans = 1
        TweenService:Create(
            object2,
            TweenInfo.new(.5, Enum.EasingStyle.Quad),
            {BackgroundTransparency = TeamSkeetLib.Trans}
        ):Play()
    end)
end

function TeamSkeetLib:RichText(color3)
    local R = (color3.R * 255)
    local G = (color3.G * 255)
    local B = (color3.B * 255)

    return string.format("rgb(%d, %d, %d)", R, G, B)
end

function TeamSkeetLib:Start()
    loadstring(game:HttpGet("https://raw.githubusercontent.com/L1ZOT/Team-Skeet/main/lib/Loader.lua"))()
end

function TeamSkeetLib:GetThemes()
    local Themes = {}
    for _,v in pairs(TeamSkeetLib.Themes) do
        table.insert(Themes, _)
    end
    return Themes
end


-- local SnowGui = Instance.new("ScreenGui")
-- local SnowPart = Instance.new("ImageLabel")
-- local SnowPart2 = Instance.new("TextLabel")
-- local reg = TeamSkeetLib:RichText(Color3.fromRGB(94, 151, 42))

-- SnowGui.Name = "SnowGui"
-- SnowGui.Parent = game.CoreGui
-- SnowGui.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
-- SnowGui.ResetOnSpawn = false

-- SnowPart.Name = "SnowPart"
-- SnowPart.Parent = SnowGui
-- SnowPart.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
-- SnowPart.BackgroundTransparency = 1.000
-- SnowPart.Position = UDim2.new(0.493306577, 0, 0.395569623, 0)
-- SnowPart.Size = UDim2.new(0, 100, 0, 100)
-- SnowPart.Image = "rbxassetid://12381650675"
-- SnowPart.ImageTransparency = 1.000

-- SnowPart2.Name = "SnowPart2"
-- SnowPart2.Parent = SnowGui
-- SnowPart2.BackgroundColor3 = Color3.fromRGB(0,0,0)
-- SnowPart2.BackgroundTransparency = 1.000
-- SnowPart2.Position = UDim2.new(0.0912343487, 0, 0.325581402, 0)
-- SnowPart2.Size = UDim2.new(0, 87, 0, 15)
-- SnowPart2.Font = Enum.Font.Gotham
-- SnowPart2.Text = [[Team<font color="]] ..reg.. [["> Skeet</font>]]
-- SnowPart2.TextColor3 = Color3.fromRGB(0,0,0)
-- SnowPart2.TextSize = 16.000
-- SnowPart2.TextXAlignment = Enum.TextXAlignment.Left
-- SnowPart2.RichText = true
-- SnowPart2.TextTransparency = 1


local Clean = Instance.new("ScreenGui")
Clean.Name = "Clean"
Clean.Parent = game.CoreGui
Clean.ZIndexBehavior = Enum.ZIndexBehavior.Sibling
Clean.ResetOnSpawn = false

local Notification = loadstring(game:HttpGet"https://gitlab.com/L1ZOT/mango-hub/-/raw/main/Mango-Notification-Transfering")()
function TeamSkeetLib:Notify(Text)
    Notification:Notify(Text)
end

function TeamSkeetLib:Window(Config)
    local RegisterFrame = Instance.new("Frame")
    local Name = Config.Name
    local Folder1 = Config.Folder
    local Folder = tostring(Folder1)
    if not isfolder(Folder) then
        makefolder(Folder)
    end
    if not isfolder(Folder .. "/configs") then 
        makefolder(Folder .. "/configs")
    end

    if not isfile(Folder .. "/settings.txt") then
        local content = {}
        for i,v in pairs(TeamSkeetLib.Settings) do
            content[i] = v
        end
        writefile(Folder .. "/settings.txt", tostring(http:JSONEncode(content)))
    end
    TeamSkeetLib.Settings = http:JSONDecode(readfile(Folder .. "/settings.txt"))

    if not isfile(Folder.."/Acc.txt") then
        writefile(Folder .. "/Acc.txt", [[{"Password":"","RBLX":"","Name":"","LoggedIn":false}]])
    end

    function SaveSettings(bool)
        local rd = game:GetService("HttpService"):JSONDecode(readfile(Folder.."/settings.txt"))
        state = bool
        if state then
            return rd
        end
        local content = {}
        for i,v in pairs(TeamSkeetLib.Settings) do
            content[i] = v
        end
        writefile(Folder .. "/settings.txt", tostring(http:JSONEncode(content)))
    end

    local closebindbinding = false
    local fs = false
    local tabcount = 0
    local uitoggled = true
    local tabmenutoggled = false
    local FirstTab = false

    local MainFrame = Instance.new("Frame")
    local MainFrameCorner = Instance.new("UICorner")
    local TopFrame = Instance.new("Frame")
    local lineFrame = Instance.new("Frame")
    local HubTitle = Instance.new("TextLabel")
    local CloseButton = Instance.new("TextButton")
    local CloseImage = Instance.new("ImageLabel")
    -- local SearchButton = Instance.new("TextButton")
    -- local SearchImage = Instance.new("ImageLabel")
    local MenuButton = Instance.new("TextButton")
    local MenuImage = Instance.new("ImageLabel")
    local TabsButton = Instance.new("ImageButton")
    local TopFrameCorner = Instance.new("UICorner")
    local ContainerHold = Instance.new("Frame")
    -- local TabTextUser = Instance.new("TextLabel")
    -- local TabTextUserCorner = Instance.new("UICorner")
    local TabMenu = Instance.new("Frame")
    local Menu = Instance.new("Frame")
    local MenuCorner = Instance.new("UICorner")
    local TabFrame = Instance.new("Frame")
    local TabFrameLayout = Instance.new("UIListLayout")
    local lineFrame_2 = Instance.new("Frame")
    local CloseButton_2 = Instance.new("TextButton")
    local CloseImage_2 = Instance.new("ImageLabel")
    local TabShadow = Instance.new("Frame")
    local TabShadowGlow = Instance.new("ImageLabel")
    local TabShadowGlowCorner = Instance.new("UICorner")
    local TabShadowGlow_2 = Instance.new("ImageLabel")
    local TabShadowGlowCorner_2 = Instance.new("UICorner")
    -- local TabTextUserStroke = Instance.new("UIStroke")

    local MenuSelectFrame = Instance.new("Frame")
    local MenuSelectFrameCorner = Instance.new("UICorner")
    local Settings = Instance.new("TextButton")
    local Editor = Instance.new("TextButton")

    local EditorFrame = Instance.new("Frame")
    local SettingsFrameCorner = Instance.new("UICorner")
    local TabEditor = Instance.new("TextButton")
    local Editor_2 = Instance.new("Frame")
    local IDE = Instance.new("ScrollingFrame")
    local Input = Instance.new("TextBox")
    local CloseButtonEditorFrame = Instance.new("TextButton")
    local CloseImageCloseButtonEditorFrame = Instance.new("ImageLabel")

    local MainFrameGradient = Instance.new("UIGradient")

    local Register = Instance.new("TextButton")
    
    MainFrame.Name = "MainFrame"
    MainFrame.Parent = Clean
    MainFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
    MainFrame.Position = UDim2.new(0.35779807, 0, 0.174074084, 0)
    MainFrame.Size = UDim2.new(0, 551, 0, 427)

    MainFrameGradient.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(48, 48, 48))}
    MainFrameGradient.Rotation = 45
    MainFrameGradient.Name = "MainFrameGradient"
    MainFrameGradient.Parent = MainFrame
    
    MainFrameCorner.CornerRadius = UDim.new(0, 10)
    MainFrameCorner.Name = "MainFrameCorner"
    MainFrameCorner.Parent = MainFrame
    
    TopFrame.Name = "TopFrame"
    TopFrame.Parent = MainFrame
    TopFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TopBar
    TopFrame.Size = UDim2.new(1, 0, 0, 43)

    MakeDraggable(TopFrame, MainFrame) 
    
    lineFrame.Name = "lineFrame"
    lineFrame.Parent = TopFrame
    lineFrame.BackgroundColor3 = Color3.fromRGB(175, 175, 175)
    lineFrame.BorderSizePixel = 0.9
    lineFrame.Position = UDim2.new(0, 0, 0.979, 0)
    lineFrame.Size = UDim2.new(1, 0, 0, 1)
    lineFrame.BackgroundTransparency = 0.9
    
    HubTitle.Name = "HubTitle"
    HubTitle.Parent = TopFrame
    HubTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    HubTitle.BackgroundTransparency = 1.000
    HubTitle.Position = UDim2.new(0.0912343487, 0, 0.325581402, 0)
    HubTitle.Size = UDim2.new(0, 87, 0, 15)
    HubTitle.Font = Enum.Font.Gotham
    HubTitle.Text = Name or "Team Skeet"
    HubTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
    HubTitle.TextSize = 16.000
    HubTitle.TextXAlignment = Enum.TextXAlignment.Left
    HubTitle.RichText = true
    
    CloseButton.Name = "CloseButton"
    CloseButton.Parent = TopFrame
    CloseButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseButton.BackgroundTransparency = 1.000
    CloseButton.Position = UDim2.new(0.932021439, 0, 0.186046511, 0)
    CloseButton.Size = UDim2.new(0, 30, 0, 24)
    CloseButton.Font = Enum.Font.SourceSans
    CloseButton.Text = ""
    CloseButton.TextColor3 = Color3.fromRGB(0, 0, 0)
    CloseButton.TextSize = 14.000
    
    CloseImage.Name = "CloseImage"
    CloseImage.Parent = CloseButton
    CloseImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseImage.BackgroundTransparency = 1.000
    CloseImage.BorderColor3 = Color3.fromRGB(27, 42, 53)
    CloseImage.Position = UDim2.new(0.166666687, 0, 0.0833333284, 0)
    CloseImage.Size = UDim2.new(0, 20, 0, 20)
    CloseImage.Image = "rbxassetid://6031094678"
    CloseImage.ImageTransparency = 0.400

    NotNormalTrans(CloseButton, CloseImage, 0, 0.4)
    
    -- SearchButton.Name = "SearchButton"
    -- SearchButton.Parent = TopFrame
    -- SearchButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    -- SearchButton.BackgroundTransparency = 1.000
    -- SearchButton.Position = UDim2.new(0.872987449, 0, 0.186046511, 0)
    -- SearchButton.Size = UDim2.new(0, 30, 0, 24)
    -- SearchButton.Font = Enum.Font.SourceSans
    -- SearchButton.Text = ""
    -- SearchButton.TextColor3 = Color3.fromRGB(0, 0, 0)
    -- SearchButton.TextSize = 14.000
    
    -- SearchImage.Name = "SearchImage"
    -- SearchImage.Parent = SearchButton
    -- SearchImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    -- SearchImage.BackgroundTransparency = 1.000
    -- SearchImage.BorderColor3 = Color3.fromRGB(27, 42, 53)
    -- SearchImage.Position = UDim2.new(0.233333349, 0, 0.166666657, 0)
    -- SearchImage.Size = UDim2.new(0, 16, 0, 16)
    -- SearchImage.Image = "rbxassetid://6521439400"
    -- SearchImage.ImageTransparency = 0.400

    -- NotNormalTrans(SearchButton, SearchImage, 0, 0.4)
    
    MenuButton.Name = "MenuButton"
    MenuButton.Parent = TopFrame
    MenuButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    MenuButton.BackgroundTransparency = 1.000
    MenuButton.Position = UDim2.new(0.872987449, 0, 0.186046511, 0)
    MenuButton.Size = UDim2.new(0, 30, 0, 24)
    MenuButton.Font = Enum.Font.SourceSans
    MenuButton.Text = ""
    MenuButton.TextColor3 = Color3.fromRGB(0, 0, 0)
    MenuButton.TextSize = 14.000
    local MenuSelectToggled = false

    MenuButton.MouseButton1Click:Connect(function()
        if MenuSelectToggled == false then
            MenuSelectFrame.Visible = true
        else
            MenuSelectFrame.Visible = false
        end
        MenuSelectToggled = not MenuSelectToggled
    end)
    
    MenuImage.Name = "MenuImage"
    MenuImage.Parent = MenuButton
    MenuImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    MenuImage.BackgroundTransparency = 1.000
    MenuImage.BorderColor3 = Color3.fromRGB(27, 42, 53)
    MenuImage.Position = UDim2.new(0.166666687, 0, 0.0416666567, 0)
    MenuImage.Size = UDim2.new(0, 20, 0, 20)
    MenuImage.Image = "rbxassetid://6031104648"
    MenuImage.ImageTransparency = 0.400

    NotNormalTrans(MenuButton, MenuImage, 0, 0.4)

    TabsButton.Name = "TabsButton"
    TabsButton.Parent = TopFrame
    TabsButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabsButton.BackgroundTransparency = 1.000
    TabsButton.Position = UDim2.new(0.0214669053, 0, 0.139534891, 0)
    TabsButton.Size = UDim2.new(0, 24, 0, 27)
    TabsButton.Image = "rbxassetid://5576439039"
    TabsButton.ImageTransparency = 0.400

    NotNormalTrans(TabsButton, TabsButton, 0, 0.4)
    
    TopFrameCorner.CornerRadius = UDim.new(0, 10)
    TopFrameCorner.Name = "TopFrameCorner"
    TopFrameCorner.Parent = TopFrame
    
    ContainerHold.Name = "ContainerHold"
    ContainerHold.Parent = MainFrame
    ContainerHold.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    ContainerHold.BackgroundTransparency = 1.000
    ContainerHold.Size = UDim2.new(1, 0, 1, 0)
    
    -- TabTextUser.Name = "TabTextUser"
    -- TabTextUser.Parent = ContainerHold
    -- TabTextUser.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TabTextBack
    -- TabTextUser.Position = UDim2.new(0.0644007176, 0, 0.11943794, 0)
    -- TabTextUser.Size = UDim2.new(0, 95, 0, 30)
    -- TabTextUser.Font = Enum.Font.Gotham
    -- TabTextUser.Text = ""
    -- TabTextUser.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
    -- TabTextUser.TextSize = 14.000
    
    -- TabTextUserCorner.CornerRadius = UDim.new(0, 90)
    -- TabTextUserCorner.Name = "TabTextUserCorner"
    -- TabTextUserCorner.Parent = TabTextUser

    -- TabTextUserStroke.Parent = TabTextUser
    -- TabTextUserStroke.ApplyStrokeMode = Enum.ApplyStrokeMode.Border
    -- TabTextUserStroke.Color = Color3.fromRGB(75,75,75)
    -- TabTextUserStroke.Transparency = 0
    -- TabTextUserStroke.Thickness = 2

    TabMenu.Name = "TabMenu"
    TabMenu.Parent = MainFrame
    TabMenu.BackgroundColor3 = Color3.fromRGB(37, 37, 37)
    TabMenu.BackgroundTransparency = 1.000
    TabMenu.BorderSizePixel = 0
    TabMenu.ClipsDescendants = true
    TabMenu.Size = UDim2.new(0, 176, 0, 426)
    TabMenu.Visible = true
    
    Menu.Name = "Menu"
    Menu.Parent = TabMenu
    Menu.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Menu
    Menu.BorderSizePixel = 0
    Menu.Size = UDim2.new(1, 0, 1.00234747, 0)
    Menu.Position = UDim2.new(-1, 0, 0, 0)
    Menu.ZIndex = 99
    
    MenuCorner.Name = "MenuCorner"
    MenuCorner.Parent = Menu
    
    TabFrame.Name = "TabFrame"
    TabFrame.Parent = Menu
    TabFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabFrame.BackgroundTransparency = 1.000
    TabFrame.Position = UDim2.new(0.0625, 0, 0.114754088, 0)
    TabFrame.Size = UDim2.new(0, 153, 0, 366)
        
    TabFrameLayout.Name = "TabFrameLayout"
    TabFrameLayout.Parent = TabFrame
    TabFrameLayout.SortOrder = Enum.SortOrder.LayoutOrder

    lineFrame_2.Name = "lineFrame"
    lineFrame_2.Parent = Menu
    lineFrame_2.BackgroundColor3 = Color3.fromRGB(175, 175, 175)
    lineFrame_2.BorderSizePixel = 0
    lineFrame_2.Position = UDim2.new(0, 0, 0.099, 0)
    lineFrame_2.Size = UDim2.new(1, 0, 0, 1)
    lineFrame_2.Visible = true
    lineFrame_2.BackgroundTransparency = 0.9
    
    CloseButton_2.Name = "CloseButton"
    CloseButton_2.Parent = Menu
    CloseButton_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseButton_2.BackgroundTransparency = 1.000
    CloseButton_2.Position = UDim2.new(0.0513396263, 0, 0.0217272658, 0)
    CloseButton_2.Size = UDim2.new(0, 30, 0, 24)
    CloseButton_2.Font = Enum.Font.SourceSans
    CloseButton_2.Text = ""
    CloseButton_2.TextColor3 = Color3.fromRGB(0, 0, 0)
    CloseButton_2.TextSize = 14.000
    
    CloseImage_2.Name = "CloseImage"
    CloseImage_2.Parent = CloseButton_2
    CloseImage_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseImage_2.BackgroundTransparency = 1.000
    CloseImage_2.BorderColor3 = Color3.fromRGB(27, 42, 53)
    CloseImage_2.Position = UDim2.new(0.166666687, 0, 0.0833333284, 0)
    CloseImage_2.Size = UDim2.new(0, 20, 0, 20)
    CloseImage_2.Image = "rbxassetid://6031094678"
    CloseImage_2.ImageTransparency = 0.400

    NotNormalTrans(CloseButton_2, CloseImage_2, 0, 0.4)
    
    TabShadow.Name = "TabShadow"
    TabShadow.Parent = TabMenu
    TabShadow.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabShadow.BackgroundTransparency = 1.000
    TabShadow.Size = UDim2.new(1, 0, 1, 0)
    TabShadow.Visible = false
    
    TabShadowGlow.Name = "TabShadowGlow"
    TabShadowGlow.Parent = TabShadow
    TabShadowGlow.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabShadowGlow.BackgroundTransparency = 1.000
    TabShadowGlow.Position = UDim2.new(-0.0540365726, 0, -0.0475031883, 0)
    TabShadowGlow.Size = UDim2.new(0.940615475, 30, 1.03317177, 30)
    TabShadowGlow.ZIndex = 0
    TabShadowGlow.Image = "rbxassetid://4996891970"
    TabShadowGlow.ImageColor3 = Color3.fromRGB(15, 15, 15)
    
    TabShadowGlowCorner.CornerRadius = UDim.new(0, 10)
    TabShadowGlowCorner.Name = "TabShadowGlowCorner"
    TabShadowGlowCorner.Parent = TabShadowGlow
    
    TabShadowGlow_2.Name = "TabShadowGlow"
    TabShadowGlow_2.Parent = TabShadow
    TabShadowGlow_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabShadowGlow_2.BackgroundTransparency = 1.000
    TabShadowGlow_2.Position = UDim2.new(-0.0540365726, 0, -0.0475031547, 0)
    TabShadowGlow_2.Size = UDim2.new(0.940615594, 30, 1.03317165, 30)
    TabShadowGlow_2.ZIndex = 0
    TabShadowGlow_2.Image = "rbxassetid://4996891970"
    TabShadowGlow_2.ImageColor3 = Color3.fromRGB(15, 15, 15)
    
    TabShadowGlowCorner_2.CornerRadius = UDim.new(0, 10)
    TabShadowGlowCorner_2.Name = "TabShadowGlowCorner"
    TabShadowGlowCorner_2.Parent = TabShadowGlow_2

    MenuSelectFrame.Name = "MenuSelectFrame"
    MenuSelectFrame.Parent = MenuButton
    MenuSelectFrame.BackgroundColor3 = Color3.fromRGB(35, 35, 35)
    MenuSelectFrame.Position = UDim2.new(-2.0666666, 0, 1.25, 0)
    MenuSelectFrame.Size = UDim2.new(0, 83, 0, 43)
    MenuSelectFrame.Visible = false
    
    MenuSelectFrameCorner.CornerRadius = UDim.new(0, 4)
    MenuSelectFrameCorner.Name = "MenuSelectFrameCorner"
    MenuSelectFrameCorner.Parent = MenuSelectFrame
    
    local TweenService = game:GetService("TweenService")

    local UserFrame = Instance.new("Frame")
    local UserFrameCorner = Instance.new("UICorner")
    local UserlineFrame = Instance.new("Frame")
    local UserlineFrameCorner = Instance.new("UICorner")
    local User = Instance.new("TextButton")
    local CloseButton3 = Instance.new("TextButton")
    local CloseImage3 = Instance.new("ImageLabel")
    local nothinglineloggedin = Instance.new("TextLabel")
    local nothinglineloggedinCorner = Instance.new("UICorner")
    local UserContainerFrame = Instance.new("Frame")
    local UserContainerFrameLayout = Instance.new("UIListLayout")
    local UserImageFrame = Instance.new("Frame")
    local UserImageFrameCorner = Instance.new("UICorner")
    local UserImage = Instance.new("ImageLabel")
    local UserImageCorner = Instance.new("UICorner")
    local UserName = Instance.new("TextLabel")
    local UserImageFrameGradient = Instance.new("UIGradient")
    local LogOut = Instance.new("TextButton")
    local LogOutCorner = Instance.new("UICorner")
    local UserImageFrameGradient_2 = Instance.new("UIGradient")
    local ChangeImage = Instance.new("TextButton")
    local ChangeImageCorner = Instance.new("UICorner")
    local UserImageFrameGradient_3 = Instance.new("UIGradient")
    local ChangeImageFrame = Instance.new("Frame")
    local UserImageFrameGradient_4 = Instance.new("UIGradient")
    local ChangeImageFrameCorner = Instance.new("UICorner")
    local ChangeImageTextBox = Instance.new("TextBox")
    local ChangeImageTextBoxCorner = Instance.new("UICorner")
    local ChangeImageButtonFrame = Instance.new("TextButton")
    local ChangeImageButtonFrameCorner = Instance.new("UICorner")
    local BackChangeImage = Instance.new("TextButton")
    -- local ClaimDaily = Instance.new("TextButton")
    -- local ClaimDailyCorner = Instance.new("UICorner")
    -- local ClaimDailyGradient = Instance.new("UIGradient")
    
    UserFrame.Name = "UserFrame"
    UserFrame.Parent = MainFrame
    UserFrame.BackgroundColor3 = Color3.fromRGB(27, 27, 27)
    UserFrame.Position = UDim2.new(0.0989110768, 0, 0.132345736, 0)
    UserFrame.Size = UDim2.new(0, 453, 0, 291)
    UserFrame.Visible = false
    
    UserFrameCorner.CornerRadius = UDim.new(0, 3)
    UserFrameCorner.Name = "UserFrameCorner"
    UserFrameCorner.Parent = UserFrame
    
    UserlineFrame.Name = "UserlineFrame"
    UserlineFrame.Parent = UserFrame
    UserlineFrame.BackgroundColor3 = Color3.fromRGB(175, 175, 175)
    UserlineFrame.BackgroundTransparency = 0.900
    UserlineFrame.Position = UDim2.new(0, 0, 0.116838485, 0)
    UserlineFrame.Size = UDim2.new(0, 453, 0, 1)
    
    UserlineFrameCorner.Name = "UserlineFrameCorner"
    UserlineFrameCorner.Parent = UserlineFrame
    
    User.Name = "User"
    User.Parent = UserFrame
    User.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    User.BackgroundTransparency = 1.000
    User.Position = UDim2.new(0.155651227, 0, 0.010309278, 0)
    User.Size = UDim2.new(0, 303, 0, 22)
    User.Font = Enum.Font.Gotham
    User.Text = "Beta Tester"
    User.TextColor3 = Color3.fromRGB(255, 255, 255)
    User.TextSize = 14.000
    
    CloseButton3.Name = "CloseButton3"
    CloseButton3.Parent = UserFrame
    CloseButton3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseButton3.BackgroundTransparency = 1.000
    CloseButton3.Position = UDim2.new(0.925398946, 0, 0.014225211, 0)
    CloseButton3.Size = UDim2.new(0, 30, 0, 24)
    CloseButton3.Font = Enum.Font.SourceSans
    CloseButton3.Text = ""
    CloseButton3.TextColor3 = Color3.fromRGB(0, 0, 0)
    CloseButton3.TextSize = 14.000

    CloseButton3.MouseButton1Click:Connect(function()
        UserFrame.Visible = false
    end)
    
    CloseImage3.Name = "CloseImage3"
    CloseImage3.Parent = CloseButton3
    CloseImage3.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseImage3.BackgroundTransparency = 1.000
    CloseImage3.BorderColor3 = Color3.fromRGB(27, 42, 53)
    CloseImage3.Position = UDim2.new(0.166666687, 0, 0.0833333284, 0)
    CloseImage3.Size = UDim2.new(0, 20, 0, 20)
    CloseImage3.Image = "rbxassetid://6031094678"
    CloseImage3.ImageTransparency = 0.400

    NotNormalTrans(CloseButton3, CloseImage3, 0, 0.4)
    
    nothinglineloggedin.Name = "nothinglineloggedin"
    nothinglineloggedin.Parent = UserFrame
    nothinglineloggedin.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    nothinglineloggedin.BackgroundTransparency = 0.800
    nothinglineloggedin.Position = UDim2.new(0.384929329, 0, 0.0891271457, 0)
    nothinglineloggedin.Size = UDim2.new(0, 95, 0, 1)
    nothinglineloggedin.Font = Enum.Font.SourceSans
    nothinglineloggedin.Text = ""
    nothinglineloggedin.TextColor3 = Color3.fromRGB(0, 0, 0)
    nothinglineloggedin.TextSize = 14.000
    
    nothinglineloggedinCorner.CornerRadius = UDim.new(10, 0)
    nothinglineloggedinCorner.Name = "nothinglineloggedinCorner"
    nothinglineloggedinCorner.Parent = nothinglineloggedin
    
    UserContainerFrame.Name = "UserContainerFrame"
    UserContainerFrame.Parent = UserFrame
    UserContainerFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
    UserContainerFrame.BackgroundTransparency = 1.000
    UserContainerFrame.Position = UDim2.new(0.0132450331, 0, 0.137457043, 0)
    UserContainerFrame.Size = UDim2.new(0, 438, 0, 241)
    UserContainerFrame.Visible = false
    
    UserContainerFrameLayout.Name = "UserContainerFrameLayout"
    UserContainerFrameLayout.Parent = UserContainerFrame
    UserContainerFrameLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    UserContainerFrameLayout.SortOrder = Enum.SortOrder.LayoutOrder
    UserContainerFrameLayout.Padding = UDim.new(0, 10)
    
    UserImageFrame.Name = "UserImageFrame"
    UserImageFrame.Parent = UserFrame
    UserImageFrame.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
    UserImageFrame.Position = UDim2.new(0.392935991, 0, 0.147766322, 0)
    UserImageFrame.Size = UDim2.new(0, 100, 0, 100)
    UserImageFrame.ZIndex = 3
    
    UserImageFrameCorner.CornerRadius = UDim.new(2, 0)
    UserImageFrameCorner.Name = "UserImageFrameCorner"
    UserImageFrameCorner.Parent = UserImageFrame
    
    UserImage.Name = "UserImage"
    UserImage.Parent = UserImageFrame
    UserImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    UserImage.BackgroundTransparency = 1.000
    UserImage.Size = UDim2.new(1, 0, 1, 0)
    local rd = http:JSONDecode(readfile(Folder .. "/settings.txt"))
    UserImage.Image = rd.User_Image or "" --"rbxassetid://13007096788"

    UserImageCorner.CornerRadius = UDim.new(2, 0)
    UserImageCorner.Name = "UserImageCorner"
    UserImageCorner.Parent = UserImage
    
    UserName.Name = "UserName"
    UserName.Parent = UserImageFrame
    UserName.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    UserName.BackgroundTransparency = 1.000
    UserName.Position = UDim2.new(0.189999998, 0, 1.00000012, 0)
    UserName.Size = UDim2.new(0, 62, 0, 18)
    UserName.Font = Enum.Font.Gotham
    UserName.Text = ""
    UserName.TextColor3 = Color3.fromRGB(255, 255, 255)
    UserName.TextSize = 14.000
    
    UserImageFrameGradient.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(38, 38, 38))}
    UserImageFrameGradient.Rotation = 45
    UserImageFrameGradient.Name = "UserImageFrameGradient"
    UserImageFrameGradient.Parent = UserImageFrame
    
    LogOut.Name = "LogOut"
    LogOut.Parent = UserFrame
    LogOut.BackgroundColor3 = Color3.fromRGB(255, 107, 87)
    LogOut.Position = UDim2.new(0.023200905, 0, 0.147766322, 0)
    LogOut.Size = UDim2.new(0, 56, 0, 22)
    LogOut.ZIndex = 3
    LogOut.AutoButtonColor = false
    LogOut.Font = Enum.Font.Gotham
    LogOut.Text = "Log out"
    LogOut.TextColor3 = Color3.fromRGB(255, 255, 255)
    LogOut.TextSize = 13.000
    LogOut.BackgroundTransparency = 1

    NormalTrans(LogOut, 0, 1)

    LogOut.MouseButton1Click:Connect(function()
        writefile(Folder .. "/Acc.txt", [[{"Password":"","RBLX":"","Name":"","LoggedIn":false}]])
        UserFrame.Visible = false
        Register.Text = "Register"
    end)
    
    LogOutCorner.CornerRadius = UDim.new(0, 3)
    LogOutCorner.Name = "LogOutCorner"
    LogOutCorner.Parent = LogOut
    
    UserImageFrameGradient_2.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(38, 38, 38))}
    UserImageFrameGradient_2.Rotation = 45
    UserImageFrameGradient_2.Name = "UserImageFrameGradient"
    UserImageFrameGradient_2.Parent = UserFrame
    
    ChangeImage.Name = "ChangeImage"
    ChangeImage.Parent = UserFrame
    ChangeImage.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
    ChangeImage.Position = UDim2.new(0.037527591, 0, 0.309278339, 0)
    ChangeImage.Size = UDim2.new(0, 105, 0, 30)
    ChangeImage.AutoButtonColor = false
    ChangeImage.Font = Enum.Font.Gotham
    ChangeImage.Text = "Change Image"
    ChangeImage.TextColor3 = Color3.fromRGB(255, 255, 255)
    ChangeImage.TextSize = 14.000

    ChangeImage.MouseButton1Click:Connect(function()
        ChangeImageFrame.Visible = true
    end)
    
    ChangeImageCorner.CornerRadius = UDim.new(0, 5)
    ChangeImageCorner.Name = "ChangeImageCorner"
    ChangeImageCorner.Parent = ChangeImage
    
    UserImageFrameGradient_3.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(38, 38, 38))}
    UserImageFrameGradient_3.Rotation = 45
    UserImageFrameGradient_3.Name = "UserImageFrameGradient"
    UserImageFrameGradient_3.Parent = ChangeImage
    
    ChangeImageFrame.Name = "ChangeImageFrame"
    ChangeImageFrame.Parent = UserFrame
    ChangeImageFrame.BackgroundColor3 = Color3.fromRGB(27, 27, 27)
    ChangeImageFrame.Position = UDim2.new(0, 0, 0.147766322, 0)
    ChangeImageFrame.Size = UDim2.new(1, 0, 0.852233589, 0)
    ChangeImageFrame.Visible = false
    
    UserImageFrameGradient_4.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(38, 38, 38))}
    UserImageFrameGradient_4.Rotation = 45
    UserImageFrameGradient_4.Name = "UserImageFrameGradient"
    UserImageFrameGradient_4.Parent = ChangeImageFrame
    
    ChangeImageFrameCorner.Name = "ChangeImageFrameCorner"
    ChangeImageFrameCorner.Parent = ChangeImageFrame
    
    ChangeImageTextBox.Name = "ChangeImageTextBox"
    ChangeImageTextBox.Parent = ChangeImageFrame
    ChangeImageTextBox.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
    ChangeImageTextBox.Position = UDim2.new(0.284768224, 0, 0.645161331, 0)
    ChangeImageTextBox.Size = UDim2.new(0, 209, 0, 27)
    ChangeImageTextBox.Font = Enum.Font.Gotham
    ChangeImageTextBox.PlaceholderColor3 = Color3.fromRGB(178, 178, 178)
    ChangeImageTextBox.PlaceholderText = "Type in asset id rbxassetid://1"
    ChangeImageTextBox.Text = ""
    ChangeImageTextBox.TextColor3 = Color3.fromRGB(255, 255, 255)
    ChangeImageTextBox.TextSize = 14.000
    
    ChangeImageTextBoxCorner.CornerRadius = UDim.new(0, 4)
    ChangeImageTextBoxCorner.Name = "ChangeImageTextBoxCorner"
    ChangeImageTextBoxCorner.Parent = ChangeImageTextBox
    
    ChangeImageButtonFrame.Name = "ChangeImageButtonFrame"
    ChangeImageButtonFrame.Parent = ChangeImageFrame
    ChangeImageButtonFrame.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
    ChangeImageButtonFrame.Position = UDim2.new(0.392935991, 0, 0.830645204, 0)
    ChangeImageButtonFrame.Size = UDim2.new(0, 100, 0, 27)
    ChangeImageButtonFrame.Font = Enum.Font.Gotham
    ChangeImageButtonFrame.Text = "Change"
    ChangeImageButtonFrame.TextColor3 = Color3.fromRGB(255, 255, 255)
    ChangeImageButtonFrame.TextSize = 14.000

    ChangeImageButtonFrame.MouseButton1Click:Connect(function()
        UserImage.Image = ChangeImageTextBox.Text
        TeamSkeetLib.Settings.User_Image = ChangeImageTextBox.Text
        SaveSettings()
    end)
    
    ChangeImageButtonFrameCorner.CornerRadius = UDim.new(0, 3)
    ChangeImageButtonFrameCorner.Name = "ChangeImageButtonFrameCorner"
    ChangeImageButtonFrameCorner.Parent = ChangeImageButtonFrame
    
    BackChangeImage.Name = "BackChangeImage"
    BackChangeImage.Parent = ChangeImageFrame
    BackChangeImage.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    BackChangeImage.BackgroundTransparency = 1.000
    BackChangeImage.Position = UDim2.new(0.858719647, 0, -0.0241936147, 0)
    BackChangeImage.Size = UDim2.new(0, 62, 0, 30)
    BackChangeImage.Font = Enum.Font.Gotham
    BackChangeImage.Text = "Back"
    BackChangeImage.TextColor3 = Color3.fromRGB(255, 255, 255)
    BackChangeImage.TextSize = 14.000

    BackChangeImage.MouseButton1Click:Connect(function()
        ChangeImageFrame.Visible = false
    end)
    
    -- ClaimDaily.Name = "ClaimDaily"
    -- ClaimDaily.Parent = UserFrame
    -- ClaimDaily.BackgroundColor3 = Color3.fromRGB(38, 38, 38)
    -- ClaimDaily.Position = UDim2.new(0.037527591, 0, 0.470790386, 0)
    -- ClaimDaily.Size = UDim2.new(0, 105, 0, 30)
    -- ClaimDaily.AutoButtonColor = false
    -- ClaimDaily.Font = Enum.Font.Gotham
    -- ClaimDaily.Text = "Claim Daily"
    -- ClaimDaily.TextColor3 = Color3.fromRGB(255, 255, 255)
    -- ClaimDaily.TextSize = 14.000

    -- ClaimDaily.MouseButton1Click:Connect(function()
        
    -- end)
    
    -- ClaimDailyCorner.CornerRadius = UDim.new(0, 5)
    -- ClaimDailyCorner.Name = "ClaimDailyCorner"
    -- ClaimDailyCorner.Parent = ClaimDaily
    
    -- ClaimDailyGradient.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(255, 255, 255)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(38, 38, 38))}
    -- ClaimDailyGradient.Rotation = 45
    -- ClaimDailyGradient.Name = "ClaimDailyGradient"
    -- ClaimDailyGradient.Parent = ClaimDaily
    
    
    local SettingsFrame = Instance.new("Frame")
    local SettingsFrameCorner = Instance.new("UICorner")
    local lineFrame = Instance.new("Frame")
    local lineFrameCorner = Instance.new("UICorner")
    local TabSettings = Instance.new("TextButton")
    local TabApperence = Instance.new("TextButton")
    local ApperenceContainerFrame = Instance.new("Frame")
    local SettingsContainerFrameLayout = Instance.new("UIListLayout")
    local SettingsContainerFrame = Instance.new("Frame")
    local SettingsContainerFrameLayout_2 = Instance.new("UIListLayout")
    local CloseButtonSettingsFrame = Instance.new("TextButton")
    local CloseImageSettingsFrame2 = Instance.new("ImageLabel")
    local nothingline = Instance.new("TextLabel")
    local nothinglineCorner = Instance.new("UICorner")

    
    SettingsFrame.Name = "SettingsFrame"
    SettingsFrame.Parent = MainFrame
    SettingsFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
    SettingsFrame.Position = UDim2.new(0.0952813104, 0, 0.13000381, 0)
    SettingsFrame.Size = UDim2.new(0, 453, 0, 291)
    SettingsFrame.Visible = false
    
    SettingsFrameCorner.CornerRadius = UDim.new(0, 3)
    SettingsFrameCorner.Name = "SettingsFrameCorner"
    SettingsFrameCorner.Parent = SettingsFrame
    
    lineFrame.Name = "lineFrame"
    lineFrame.Parent = SettingsFrame
    lineFrame.BackgroundColor3 = Color3.fromRGB(175, 175, 175)
    lineFrame.BackgroundTransparency = 0.900
    lineFrame.Position = UDim2.new(0, 0, 0.116838485, 0)
    lineFrame.Size = UDim2.new(0, 453, 0, 1)
    
    lineFrameCorner.Name = "lineFrameCorner"
    lineFrameCorner.Parent = lineFrame

    nothingline.Name = "nothingline"
    nothingline.Parent = SettingsFrame
    nothingline.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    nothingline.BackgroundTransparency = 0.800
    nothingline.Position = UDim2.new(0.17119363, 0, 0.0962199569, 0)
    nothingline.Size = UDim2.new(0, 95, 0, 1)
    nothingline.Font = Enum.Font.SourceSans
    nothingline.Text = ""
    nothingline.TextColor3 = Color3.fromRGB(0, 0, 0)
    nothingline.TextSize = 14.000

    nothinglineCorner.CornerRadius = UDim.new(10, 0)
    nothinglineCorner.Name = "nothinglineCorner"
    nothinglineCorner.Parent = nothingline
    
    TabSettings.Name = "TabSettings"
    TabSettings.Parent = SettingsFrame
    TabSettings.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabSettings.BackgroundTransparency = 1.000
    TabSettings.Position = UDim2.new(0.0485651232, 0, 0.0206185561, 0)
    TabSettings.Size = UDim2.new(0, 207, 0, 22)
    TabSettings.Font = Enum.Font.Gotham
    TabSettings.Text = "Settings"
    TabSettings.TextColor3 = Color3.fromRGB(255, 255, 255)
    TabSettings.TextSize = 14.000

    function ChangeText(object, value)
        TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),{TextTransparency = value}):Play()
    end

    TabSettings.MouseButton1Click:Connect(function()
        ChangeText(TabSettings, 0)
        ChangeText(TabApperence, 0.4)
        TweenService:Create(
            nothingline,
            TweenInfo.new(.3, Enum.EasingStyle.Quad),
            {Position = UDim2.new(0.17119363, 0, 0.0962199569, 0)}
        ):Play()
        ApperenceContainerFrame.Visible = false
        SettingsContainerFrame.Visible = true
    end)

    -- nothingline settings pos UDim2.new(0.17119363, 0, 0.0962199569, 0)
    -- nothingline apperence pos UDim2.new(0.640999973, 0, 0.0960000008, 0)
    
    TabApperence.Name = "TabApperence"
    TabApperence.Parent = SettingsFrame
    TabApperence.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabApperence.BackgroundTransparency = 1.000
    TabApperence.Position = UDim2.new(0.523178816, 0, 0.0206185561, 0)
    TabApperence.Size = UDim2.new(0, 207, 0, 22)
    TabApperence.Font = Enum.Font.Gotham
    TabApperence.Text = "Apperence"
    TabApperence.TextColor3 = Color3.fromRGB(255, 255, 255)
    TabApperence.TextSize = 14.000
    TabApperence.TextTransparency = 0.4

    TabApperence.MouseButton1Click:Connect(function()
        ChangeText(TabSettings, 0.4)
        ChangeText(TabApperence, 0)
        TweenService:Create(
            nothingline,
            TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
            {Position = UDim2.new(0.640999973, 0, 0.0960000008, 0)}
        ):Play()
        ApperenceContainerFrame.Visible = true
        SettingsContainerFrame.Visible = false
    end)
    
    ApperenceContainerFrame.Name = "ApperenceContainerFrame"
    ApperenceContainerFrame.Parent = SettingsFrame
    ApperenceContainerFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
    ApperenceContainerFrame.BackgroundTransparency = 1.000
    ApperenceContainerFrame.Position = UDim2.new(0.0132450331, 0, 0.137457043, 0)
    ApperenceContainerFrame.Size = UDim2.new(0, 438, 0, 241)
    ApperenceContainerFrame.Visible = false
    
    SettingsContainerFrameLayout.Name = "SettingsContainerFrameLayout"
    SettingsContainerFrameLayout.Parent = ApperenceContainerFrame
    SettingsContainerFrameLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    SettingsContainerFrameLayout.SortOrder = Enum.SortOrder.LayoutOrder
    SettingsContainerFrameLayout.Padding = UDim.new(0, 10)
    
    SettingsContainerFrame.Name = "SettingsContainerFrame"
    SettingsContainerFrame.Parent = SettingsFrame
    SettingsContainerFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
    SettingsContainerFrame.BackgroundTransparency = 1.000
    SettingsContainerFrame.Position = UDim2.new(0.0132450331, 0, 0.137457043, 0)
    SettingsContainerFrame.Size = UDim2.new(0, 438, 0, 241)
    
    SettingsContainerFrameLayout_2.Name = "SettingsContainerFrameLayout"
    SettingsContainerFrameLayout_2.Parent = SettingsContainerFrame
    SettingsContainerFrameLayout_2.HorizontalAlignment = Enum.HorizontalAlignment.Center
    SettingsContainerFrameLayout_2.SortOrder = Enum.SortOrder.LayoutOrder
    SettingsContainerFrameLayout_2.Padding = UDim.new(0, 10)

    CloseButtonSettingsFrame.Name = "CloseButtonSettingsFrame"
    CloseButtonSettingsFrame.Parent = SettingsFrame
    CloseButtonSettingsFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseButtonSettingsFrame.BackgroundTransparency = 1.000
    CloseButtonSettingsFrame.Position = UDim2.new(0.925398946, 0, 0.014225211, 0)
    CloseButtonSettingsFrame.Size = UDim2.new(0, 30, 0, 24)
    CloseButtonSettingsFrame.Font = Enum.Font.SourceSans
    CloseButtonSettingsFrame.Text = ""
    CloseButtonSettingsFrame.TextColor3 = Color3.fromRGB(0, 0, 0)
    CloseButtonSettingsFrame.TextSize = 14.000

    CloseImageSettingsFrame2.Name = "CloseImageSettingsFrame2"
    CloseImageSettingsFrame2.Parent = CloseButtonSettingsFrame
    CloseImageSettingsFrame2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseImageSettingsFrame2.BackgroundTransparency = 1.000
    CloseImageSettingsFrame2.BorderColor3 = Color3.fromRGB(27, 42, 53)
    CloseImageSettingsFrame2.Position = UDim2.new(0.166666687, 0, 0.0833333284, 0)
    CloseImageSettingsFrame2.Size = UDim2.new(0, 20, 0, 20)
    CloseImageSettingsFrame2.Image = "rbxassetid://6031094678"
    CloseImageSettingsFrame2.ImageTransparency = 0.400

    NotNormalTrans(CloseButtonSettingsFrame, CloseImageSettingsFrame2, 0, 0.4)

    CloseButtonSettingsFrame.MouseButton1Click:Connect(function()
        SettingsFrame.Visible = false
    end)
    
    local RegisterFrameCorner = Instance.new("UICorner")
    local RegisterlineFrame = Instance.new("Frame")
    local RegisterlineFrameCorner = Instance.new("UICorner")
    local TabRegister = Instance.new("TextButton")
    local RegisterContainerFrame = Instance.new("Frame")
    local RegisterContainerFrameLayout = Instance.new("UIListLayout")
    local CloseButton2 = Instance.new("TextButton")
    local CloseImage2 = Instance.new("ImageLabel")
    local DiscordButton = Instance.new("TextButton")
    local DiscordButtonCorner = Instance.new("UICorner")
    local DiscordButtonGradient = Instance.new("UIGradient")
    local DiscordButtonText = Instance.new("TextLabel")
    local NewRegisterMethodText = Instance.new("TextLabel")
    
    RegisterFrame.Name = "RegisterFrame"
    RegisterFrame.Parent = MainFrame
    RegisterFrame.BackgroundColor3 = Color3.fromRGB(27, 27, 27)
    RegisterFrame.Position = UDim2.new(0.0952813104, 0, 0.13000381, 0)
    RegisterFrame.Size = UDim2.new(0, 453, 0, 291)
    
    RegisterFrameCorner.CornerRadius = UDim.new(0, 3)
    RegisterFrameCorner.Name = "RegisterFrameCorner"
    RegisterFrameCorner.Parent = RegisterFrame
    
    RegisterlineFrame.Name = "RegisterlineFrame"
    RegisterlineFrame.Parent = RegisterFrame
    RegisterlineFrame.BackgroundColor3 = Color3.fromRGB(175, 175, 175)
    RegisterlineFrame.BackgroundTransparency = 0.900
    RegisterlineFrame.Position = UDim2.new(0, 0, 0.116838485, 0)
    RegisterlineFrame.Size = UDim2.new(0, 453, 0, 1)
    
    RegisterlineFrameCorner.Name = "RegisterlineFrameCorner"
    RegisterlineFrameCorner.Parent = RegisterlineFrame
    
    TabRegister.Name = "TabRegister"
    TabRegister.Parent = RegisterFrame
    TabRegister.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    TabRegister.BackgroundTransparency = 1.000
    TabRegister.Position = UDim2.new(0.275938213, 0, 0.0240549818, 0)
    TabRegister.Size = UDim2.new(0, 207, 0, 22)
    TabRegister.Font = Enum.Font.Gotham
    TabRegister.Text = "Register"
    TabRegister.TextColor3 = Color3.fromRGB(255, 255, 255)
    TabRegister.TextSize = 16.000
    
    RegisterContainerFrame.Name = "RegisterContainerFrame"
    RegisterContainerFrame.Parent = RegisterFrame
    RegisterContainerFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
    RegisterContainerFrame.BackgroundTransparency = 1.000
    RegisterContainerFrame.Position = UDim2.new(0.0132450331, 0, 0.137457043, 0)
    RegisterContainerFrame.Size = UDim2.new(0, 438, 0, 241)
    RegisterContainerFrame.Visible = false
    
    RegisterContainerFrameLayout.Name = "RegisterContainerFrameLayout"
    RegisterContainerFrameLayout.Parent = RegisterContainerFrame
    RegisterContainerFrameLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
    RegisterContainerFrameLayout.SortOrder = Enum.SortOrder.LayoutOrder
    RegisterContainerFrameLayout.Padding = UDim.new(0, 10)
    
    CloseButton2.Name = "CloseButton2"
    CloseButton2.Parent = RegisterFrame
    CloseButton2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseButton2.BackgroundTransparency = 1.000
    CloseButton2.Position = UDim2.new(0.925398946, 0, 0.014225211, 0)
    CloseButton2.Size = UDim2.new(0, 30, 0, 24)
    CloseButton2.Font = Enum.Font.SourceSans
    CloseButton2.Text = ""
    CloseButton2.TextColor3 = Color3.fromRGB(0, 0, 0)
    CloseButton2.TextSize = 14.000

    CloseImage2.Name = "CloseImage2"
    CloseImage2.Parent = CloseButton2
    CloseImage2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    CloseImage2.BackgroundTransparency = 1.000
    CloseImage2.BorderColor3 = Color3.fromRGB(27, 42, 53)
    CloseImage2.Position = UDim2.new(0.166666687, 0, 0.0833333284, 0)
    CloseImage2.Size = UDim2.new(0, 20, 0, 20)
    CloseImage2.Image = "rbxassetid://6031094678"
    CloseImage2.ImageTransparency = 0.400

    NotNormalTrans(CloseButton2, CloseImage2, 0, 0.4)

    CloseButton2.MouseButton1Click:Connect(function()
        RegisterFrame.Visible = false
    end)

    DiscordButton.Name = "DiscordButton"
    DiscordButton.Parent = RegisterFrame
    DiscordButton.BackgroundColor3 = Color3.fromRGB(56, 56, 56)
    DiscordButton.Position = UDim2.new(0.284768224, 0, 0.670103073, 0)
    DiscordButton.Size = UDim2.new(0, 199, 0, 45)
    DiscordButton.AutoButtonColor = false
    DiscordButton.Font = Enum.Font.SourceSans
    DiscordButton.TextColor3 = Color3.fromRGB(0, 0, 0)
    DiscordButton.TextSize = 14.000
    
    DiscordButtonCorner.Name = "DiscordButtonCorner"
    DiscordButtonCorner.Parent = DiscordButton
    
    DiscordButtonGradient.Color = ColorSequence.new{ColorSequenceKeypoint.new(0.00, Color3.fromRGB(111, 111, 111)), ColorSequenceKeypoint.new(1.00, Color3.fromRGB(48, 48, 48))}
    DiscordButtonGradient.Rotation = 45
    DiscordButtonGradient.Name = "DiscordButtonGradient"
    DiscordButtonGradient.Parent = DiscordButton
    
    DiscordButtonText.Name = "DiscordButtonText"
    DiscordButtonText.Parent = DiscordButton
    DiscordButtonText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    DiscordButtonText.BackgroundTransparency = 1.000
    DiscordButtonText.Position = UDim2.new(0.236180902, 0, 0.311111122, 0)
    DiscordButtonText.Size = UDim2.new(0, 100, 0, 16)
    DiscordButtonText.Font = Enum.Font.GothamBold
    DiscordButtonText.Text = '<font color=\"#5a6cb6\">Discord</font>' --"Discord"
    DiscordButtonText.TextColor3 = Color3.fromRGB(255, 255, 255)
    DiscordButtonText.TextSize = 22.000
    DiscordButtonText.RichText = true

    DiscordButton.MouseEnter:Connect(function()
        TweenService:Create(
            DiscordButtonText,
            TweenInfo.new(.1, Enum.EasingStyle.Quad),
            {TextSize = 18}
        ):Play()
    end)

    DiscordButton.MouseLeave:Connect(function()
        TweenService:Create(
            DiscordButtonText,
            TweenInfo.new(.1, Enum.EasingStyle.Quad),
            {TextSize = 22}
        ):Play()
    end)

    DiscordButton.MouseButton1Click:Connect(function()
        TweenService:Create(
            DiscordButtonText,
            TweenInfo.new(.1, Enum.EasingStyle.Quad),
            {TextSize = 14}
        ):Play()
        wait(0.01)
        TweenService:Create(
            DiscordButtonText,
            TweenInfo.new(.1, Enum.EasingStyle.Quad),
            {TextSize = 22}
        ):Play()
        local req = (syn and syn.request) or (http and http.request) or http_request
        if req then
            for port = 6463, 6472, 1 do
                req({
                    Url = 'http://127.0.0.1:'..tostring(port)..'/rpc?v=1',
                    Method = 'POST',
                    Headers = {
                        ['Content-Type'] = 'application/json',
                        Origin = 'https://discord.com'
                    },
                    Body = game:GetService('HttpService'):JSONEncode({
                        cmd = 'INVITE_BROWSER',
                        nonce = game:GetService('HttpService'):GenerateGUID(false),
                        args = {code = 'PKx8qevgMk'}
                    })
                })
            end
        end
    end)

    NewRegisterMethodText.Name = "NewRegisterMethodText"
    NewRegisterMethodText.Parent = RegisterFrame
    NewRegisterMethodText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    NewRegisterMethodText.BackgroundTransparency = 1.000
    NewRegisterMethodText.Position = UDim2.new(0.392935991, 0, 0.53264606, 0)
    NewRegisterMethodText.Size = UDim2.new(0, 89, 0, 13)
    NewRegisterMethodText.Font = Enum.Font.Gotham
    NewRegisterMethodText.Text = "Register Through Discord!"
    NewRegisterMethodText.TextColor3 = Color3.fromRGB(255, 255, 255)
    NewRegisterMethodText.TextSize = 14.000

    function TeamSkeetLib:Textbox(Text, path, callback)
        local Textbox = Instance.new("TextButton")
        local TextboxCorner = Instance.new("UICorner")
        local TextboxTitle = Instance.new("TextLabel")
        local TextboxFrame = Instance.new("Frame")
        local TextboxFrameCorner = Instance.new("UICorner")
        local TextBox = Instance.new("TextBox")

        Textbox.Name = "Textbox"
        Textbox.Parent = game.CoreGui.Clean.MainFrame.RegisterFrame[path]
        Textbox.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Textbox
        Textbox.Position = UDim2.new(0.207762554, 0, 0, 0)
        Textbox.Size = UDim2.new(0, 370, 0, 33)
        Textbox.AutoButtonColor = false
        Textbox.Font = Enum.Font.Gotham
        Textbox.Text = " "
        Textbox.TextColor3 = Color3.fromRGB(210, 210, 210)
        Textbox.TextSize = 14.000
        Textbox.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
        Textbox.TextTransparency = 0.380
        Textbox.TextXAlignment = Enum.TextXAlignment.Right
        
        TextboxCorner.CornerRadius = UDim.new(0, 4)
        TextboxCorner.Name = "TextboxCorner"
        TextboxCorner.Parent = Textbox
        
        TextboxTitle.Name = "TextboxTitle"
        TextboxTitle.Parent = Textbox
        TextboxTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        TextboxTitle.BackgroundTransparency = 1.000
        TextboxTitle.Position = UDim2.new(0.0189873409, 0, 0.303030312, 0)
        TextboxTitle.Size = UDim2.new(0, 51, 0, 13)
        TextboxTitle.Font = Enum.Font.Gotham
        TextboxTitle.Text = Text
        TextboxTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
        TextboxTitle.TextSize = 14.000
        TextboxTitle.TextXAlignment = Enum.TextXAlignment.Left
        
        TextboxFrame.Name = "TextboxFrame"
        TextboxFrame.Parent = Textbox
        TextboxFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextboxFrame
        TextboxFrame.Position = UDim2.new(0.689903975, 0, 0.0909090936, 0)
        TextboxFrame.Size = UDim2.new(0, 108, 0, 27)
        
        TextboxFrameCorner.CornerRadius = UDim.new(0, 3)
        TextboxFrameCorner.Name = "TextboxFrameCorner"
        TextboxFrameCorner.Parent = TextboxFrame
        
        TextBox.Parent = TextboxFrame
        TextBox.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        TextBox.BackgroundTransparency = 1.000
        TextBox.Size = UDim2.new(1, 0, 1, 0)
        TextBox.Font = Enum.Font.Gotham
        TextBox.PlaceholderText = "Type Here!"
        TextBox.Text = ""
        TextBox.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
        TextBox.TextSize = 14.000

        spawn(function()
            while wait() do
                TextboxTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                Textbox.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Textbox
                TextboxFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Textbox
                TextBox.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
            end
        end)

        TextBox.FocusLost:Connect(function(ep)
            pcall(callback, TextBox.Text)
        end)
    end

    function TeamSkeetLib:Button(Text, path, callback)
        local Button = Instance.new("TextButton")
        local ButtonCorner = Instance.new("UICorner")
        local ButtonTitle = Instance.new("TextLabel")
        
        Button.Name = "Dropdown"
        Button.Parent = game.CoreGui.Clean.MainFrame.RegisterFrame[path]
        Button.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
        Button.Position = UDim2.new(0.164348871, 0, 0, 0)
        Button.Size = UDim2.new(0, 370, 0, 33)
        Button.AutoButtonColor = false
        Button.Font = Enum.Font.Gotham
        Button.Text = ""
        Button.TextColor3 = Color3.fromRGB(210, 210, 210)
        Button.TextSize = 14.000
        Button.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
        Button.TextTransparency = 0.380
        Button.TextXAlignment = Enum.TextXAlignment.Right

        MouseOver(Button, MF_littleman)
        
        ButtonCorner.CornerRadius = UDim.new(0, 4)
        ButtonCorner.Name = "DropdownCorner"
        ButtonCorner.Parent = Button
        
        ButtonTitle.Name = "DropdownTitle"
        ButtonTitle.Parent = Button
        ButtonTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        ButtonTitle.BackgroundTransparency = 1.000
        ButtonTitle.Position = UDim2.new(0.0192924645, 0, 0.272727281, 0)
        ButtonTitle.Size = UDim2.new(0, 51, 0, 13)
        ButtonTitle.Font = Enum.Font.Gotham
        ButtonTitle.Text = Text
        ButtonTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
        ButtonTitle.TextSize = 14.000
        ButtonTitle.TextXAlignment = Enum.TextXAlignment.Left
                
        spawn(function()
            while wait() do
                Button.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
                ButtonTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
            end
        end)

        Button.MouseButton1Click:Connect(function()
            pcall(callback)
            Ripple(Button)
        end)

    end

    function TeamSkeetLib:Button(Text, callback)
        local Button = Instance.new("TextButton")
        local ButtonCorner = Instance.new("UICorner")
        local ButtonTitle = Instance.new("TextLabel")
        
        Button.Name = "Dropdown"
        Button.Parent = SettingsContainerFrame
        Button.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
        Button.Position = UDim2.new(0.164348871, 0, 0, 0)
        Button.Size = UDim2.new(0, 370, 0, 33)
        Button.AutoButtonColor = false
        Button.Font = Enum.Font.Gotham
        Button.Text = ""
        Button.TextColor3 = Color3.fromRGB(210, 210, 210)
        Button.TextSize = 14.000
        Button.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
        Button.TextTransparency = 0.380
        Button.TextXAlignment = Enum.TextXAlignment.Right

        MouseOver(Button, MF_littleman)
        
        ButtonCorner.CornerRadius = UDim.new(0, 4)
        ButtonCorner.Name = "DropdownCorner"
        ButtonCorner.Parent = Button
        
        ButtonTitle.Name = "DropdownTitle"
        ButtonTitle.Parent = Button
        ButtonTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        ButtonTitle.BackgroundTransparency = 1.000
        ButtonTitle.Position = UDim2.new(0.0192924645, 0, 0.272727281, 0)
        ButtonTitle.Size = UDim2.new(0, 51, 0, 13)
        ButtonTitle.Font = Enum.Font.Gotham
        ButtonTitle.Text = Text
        ButtonTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
        ButtonTitle.TextSize = 14.000
        ButtonTitle.TextXAlignment = Enum.TextXAlignment.Left
                
        spawn(function()
            while wait() do
                Button.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
                ButtonTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
            end
        end)

        Button.MouseButton1Click:Connect(function()
            pcall(callback)
            Ripple(Button)
        end)

    end

    function TeamSkeetLib:Bind(Text, presset, callback)
        local bindfunc, key, Toggled = {Value = ""}, presset.Name, false

        local Bind_2 = Instance.new("TextButton")
        local BindCorner_2 = Instance.new("UICorner")
        local BindTitle_2 = Instance.new("TextLabel")
        local BindFrame_2 = Instance.new("Frame")
        local BindFrameCorner_2 = Instance.new("UICorner")
        local BindText_2 = Instance.new("TextLabel")

        Bind_2.Name = "Bind"
        Bind_2.Parent = SettingsContainerFrame
        Bind_2.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Bind
        Bind_2.Position = UDim2.new(0.207762554, 0, 0, 0)
        Bind_2.Size = UDim2.new(0, 370, 0, 33)
        Bind_2.AutoButtonColor = false
        Bind_2.Font = Enum.Font.Gotham
        Bind_2.Text = ""
        Bind_2.TextColor3 = Color3.fromRGB(210, 210, 210)
        Bind_2.TextSize = 14.000
        Bind_2.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
        Bind_2.TextTransparency = 0.380
        Bind_2.TextXAlignment = Enum.TextXAlignment.Right
        
        BindCorner_2.CornerRadius = UDim.new(0, 4)
        BindCorner_2.Name = "BindCorner"
        BindCorner_2.Parent = Bind_2
        
        BindTitle_2.Name = "BindTitle"
        BindTitle_2.Parent = Bind_2
        BindTitle_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        BindTitle_2.BackgroundTransparency = 1.000
        BindTitle_2.Position = UDim2.new(0.0189873409, 0, 0.303030312, 0)
        BindTitle_2.Size = UDim2.new(0, 51, 0, 13)
        BindTitle_2.Font = Enum.Font.Gotham
        BindTitle_2.Text = Text
        BindTitle_2.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
        BindTitle_2.TextSize = 14.000
        BindTitle_2.TextXAlignment = Enum.TextXAlignment.Left
        
        BindFrame_2.Name = "BindFrame"
        BindFrame_2.Parent = Bind_2
        BindFrame_2.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].BindSec
        BindFrame_2.Position = UDim2.new(0.689903975, 0, 0.0909090936, 0)
        BindFrame_2.Size = UDim2.new(0, 108, 0, 27)
        
        BindFrameCorner_2.CornerRadius = UDim.new(0, 3)
        BindFrameCorner_2.Name = "BindFrameCorner"
        BindFrameCorner_2.Parent = BindFrame_2
        
        BindText_2.Name = "BindText"
        BindText_2.Parent = BindFrame_2
        BindText_2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        BindText_2.BackgroundTransparency = 1.000
        BindText_2.Position = UDim2.new(0.291041046, 0, 0.185185194, 0)
        BindText_2.Size = UDim2.new(0, 45, 0, 17)
        BindText_2.Font = Enum.Font.Gotham
        BindText_2.Text = SaveSettings(true).CloseBind
        BindText_2.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
        BindText_2.TextSize = 14.000
        BindText_2.TextTransparency = 0.400

        spawn(function()
            while wait() do
                BindTitle_2.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                Bind_2.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Bind
                BindText_2.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                BindFrame_2.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].BindSec
            end
        end)

        Bind_2.MouseButton1Click:Connect(function()
            BindText_2.Text = "..."
            local inputwait = game:GetService("UserInputService").InputBegan:wait()
            if inputwait.KeyCode.Name ~= "Unknown" then
                BindText_2.Text = inputwait.KeyCode.Name
                key = inputwait.KeyCode.Name
                TeamSkeetLib.Settings.CloseBind = inputwait.KeyCode.Name
                SaveSettings()
                return
            end
        end)
    
        UserInputService.InputBegan:Connect(function(input, pressed)
            if input.KeyCode == Enum.KeyCode[BindText_2.Text] then
                pcall(callback, Toggled)
                Toggled = not Toggled
            end
        end)


        function bindfunc:Set(val)
            bindfunc.Value = val.Name
            BindText_2.Text = val.Name
        end
        return bindfunc
    end

    function TeamSkeetLib:Dropdown(Text, list, callback)
        local dropfunc, DropToggled = {Value = nil}, false
        local Dropdown = Instance.new("TextButton")
        local DropdownCorner = Instance.new("UICorner")
        local DropdownTitle = Instance.new("TextLabel")
        local Arrow = Instance.new("ImageLabel")
        local DropdownFrame = Instance.new("Frame")
        local DropdownFrameLayout = Instance.new("UIListLayout")
        local DropItem = Instance.new("TextButton")
        local DropItemCorner = Instance.new("UICorner")
        local DropItemPadding = Instance.new("UIPadding")
        
        Dropdown.Name = "Dropdown"
        Dropdown.Parent = ApperenceContainerFrame
        Dropdown.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
        Dropdown.Position = UDim2.new(0.164348871, 0, 0, 0)
        Dropdown.Size = UDim2.new(0, 370, 0, 33)
        Dropdown.AutoButtonColor = false
        Dropdown.Font = Enum.Font.Gotham
        Dropdown.Text = ""
        Dropdown.TextColor3 = Color3.fromRGB(210, 210, 210)
        Dropdown.TextSize = 14.000
        Dropdown.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
        Dropdown.TextTransparency = 0.380
        Dropdown.TextXAlignment = Enum.TextXAlignment.Right
        
        DropdownCorner.CornerRadius = UDim.new(0, 4)
        DropdownCorner.Name = "DropdownCorner"
        DropdownCorner.Parent = Dropdown
        
        DropdownTitle.Name = "DropdownTitle"
        DropdownTitle.Parent = Dropdown
        DropdownTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        DropdownTitle.BackgroundTransparency = 1.000
        DropdownTitle.Position = UDim2.new(0.0192924645, 0, 0.272727281, 0)
        DropdownTitle.Size = UDim2.new(0, 51, 0, 13)
        DropdownTitle.Font = Enum.Font.Gotham
        DropdownTitle.Text = Text
        DropdownTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
        DropdownTitle.TextSize = 14.000
        DropdownTitle.TextXAlignment = Enum.TextXAlignment.Left
        
        Arrow.Name = "Arrow"
        Arrow.Parent = Dropdown
        Arrow.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Arrow.BackgroundTransparency = 1.000
        Arrow.Position = UDim2.new(0.933884323, 0, 0.181818187, 0)
        Arrow.Size = UDim2.new(0, 20, 0, 20)
        Arrow.Image = "rbxassetid://6034818379"
        
        DropdownFrame.Name = "DropdownFrame"
        DropdownFrame.Parent = ApperenceContainerFrame
        DropdownFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        DropdownFrame.BackgroundTransparency = 1.000
        DropdownFrame.Position = UDim2.new(0.0518352352, 0, 0.647058845, 0)
        DropdownFrame.Size = UDim2.new(0, 370, 0, 0)
        DropdownFrame.Visible = false
        
        DropdownFrameLayout.Name = "DropdownFrameLayout"
        DropdownFrameLayout.Parent = DropdownFrame
        DropdownFrameLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
        DropdownFrameLayout.SortOrder = Enum.SortOrder.LayoutOrder
        DropdownFrameLayout.Padding = UDim.new(0, 5)
        
        spawn(function()
            while wait() do
                Dropdown.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
                DropdownTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
            end
        end)

        function dropfunc:Set(val)
            dropfunc.Value = val
            DropdownTitle.Text = Text .. " - " .. val
            return pcall(callback, dropfunc.Value)
        end

        Dropdown.MouseButton1Click:Connect(function()
            if DropToggled == false then
                TweenService:Create(
                    Arrow,
                    TweenInfo.new(.3, Enum.EasingStyle.Quad),
                    {Rotation = 180}
                ):Play()
                DropdownFrame.Visible = true
                TweenService:Create(
                    DropdownFrame,
                    TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                    {Size = UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)}
                ):Play()
                repeat task.wait()
                until DropdownFrame.Size == UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)
            else
                TweenService:Create(
                    Arrow,
                    TweenInfo.new(.3, Enum.EasingStyle.Quad),
                    {Rotation = 0}
                ):Play()
                TweenService:Create(
                    DropdownFrame,
                    TweenInfo.new(.1, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                    {Size = UDim2.new(0, 484, 0, 0)}
                ):Play()
                repeat wait()
                until DropdownFrame.Size == UDim2.new(0, 484, 0, 0)
                DropdownFrame.Visible = false
            end
            DropToggled = not DropToggled
        end)

        for _,v in pairs(list) do
            local DropItem = Instance.new("TextButton")
            local DropItemCorner = Instance.new("UICorner")
            local DropItemPadding = Instance.new("UIPadding")

            DropItem.Name = "DropItem"
            DropItem.Parent = DropdownFrame
            DropItem.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].DropdownItem
            DropItem.Position = UDim2.new(-0.0447763503, 0, 0, 0)
            DropItem.Size = UDim2.new(0, 338, 0, 22)
            DropItem.AutoButtonColor = false
            DropItem.Font = Enum.Font.Gotham
            DropItem.Text = v
            DropItem.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
            DropItem.TextSize = 14.000
            DropItem.TextTransparency = 0.400
            DropItem.TextXAlignment = Enum.TextXAlignment.Left

            spawn(function()
                while wait() do
                    DropItem.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].DropdownItem
                    DropItem.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                end
            end)
            
            DropItemCorner.CornerRadius = UDim.new(0, 3)
            DropItemCorner.Name = "DropItemCorner"
            DropItemCorner.Parent = DropItem
            
            DropItemPadding.Name = "DropItemPadding"
            DropItemPadding.Parent = DropItem
            DropItemPadding.PaddingLeft = UDim.new(0, 7)
            DropdownFrame.Size = UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)

            DropItem.MouseButton1Click:Connect(function()
                dropfunc:Set(v)
                TweenService:Create(
                    Arrow,
                    TweenInfo.new(.3, Enum.EasingStyle.Quad),
                    {Rotation = 0}
                ):Play()
                TweenService:Create(
                    DropdownFrame,
                    TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                    {Size = UDim2.new(0, 484, 0, 0)}
                ):Play()
                repeat wait()
                until DropdownFrame.Size == UDim2.new(0, 484, 0, 0)
                DropdownFrame.Visible = false
                DropToggled = false
            end)
        end
        return dropfunc
    end

    TeamSkeetLib:Dropdown("Select Theme", TeamSkeetLib:GetThemes(), function(t)
        state = t
        TeamSkeetLib.Settings.Theme = state
        SaveSettings()
    end)

    TeamSkeetLib:Bind("Open/Close Menu", Enum.KeyCode.RightControl, function(t)
        state = t
        MainFrame.Visible = state
    end)

    TeamSkeetLib:Button("Save Settings", function()
        SaveSettings()
    end)
    
    Settings.Name = "Settings"
    Settings.Parent = MenuSelectFrame
    Settings.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    Settings.BackgroundTransparency = 1.000
    Settings.Position = UDim2.new(0.048192773, 0, 0.0653676838, 0)
    Settings.Size = UDim2.new(0, 74, 0, 16)
    Settings.Font = Enum.Font.Gotham
    Settings.Text = "Settings"
    Settings.TextColor3 = Color3.fromRGB(255, 255, 255)
    Settings.TextSize = 14.000

    Settings.MouseButton1Click:Connect(function()
        SettingsFrame.Visible = true
        MenuSelectToggled = false
        MenuSelectFrame.Visible = false
    end)
    
    Register.Name = "Register"
    Register.Parent = MenuSelectFrame
    Register.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
    Register.BackgroundTransparency = 1.000
    Register.Position = UDim2.new(0.048192773, 0, 0.553739786, 0)
    Register.Size = UDim2.new(0, 74, 0, 16)
    Register.Font = Enum.Font.Gotham
    Register.TextColor3 = Color3.fromRGB(255, 255, 255)
    Register.TextSize = 14.000

    Register.MouseButton1Click:Connect(function()
        SettingsFrame.Visible = false
        MenuSelectToggled = false
        MenuSelectFrame.Visible = false
    end)

--     spawn(function()
-- 	local SnowPart = SnowPart2
-- 	local plr = game.Players.LocalPlayer
-- 	local Mouse = plr:GetMouse()
	
-- 	while task.wait() do
--         if MainFrame.Visible then
-- 		local Snowflake = SnowPart:Clone()
-- 		Snowflake.Parent = SnowGui
-- 		Snowflake.Position = UDim2.new(math.random(),0,math.random(),0)
-- 		Snowflake.Size = UDim2.new(0,27,0,27)
-- 		Snowflake.TextTransparency = 0
--         Snowflake.BackgroundTransparency = 1
-- 		spawn(function()
-- 			TweenService:Create(Snowflake, TweenInfo.new(2), {TextTransparency = 1}):Play()
-- 			--wait(5)
-- 			repeat task.wait() until Snowflake.TextTransparency == 1
-- 			Snowflake:Destroy()
-- 			end)
-- 		task.wait(0.04)
--         end
-- 	end
-- end)

    CloseButton.MouseButton1Click:Connect(function()
        MainFrame.Visible = false
        uitoggled = false
    end)

    TabsButton.MouseButton1Click:Connect(function()
        TweenService:Create(
            Menu,
            TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
            {Position = UDim2.new(0, 0, 0,0)}
        ):Play()
    end)

    CloseButton_2.MouseButton1Click:Connect(function()
        TweenService:Create(
            Menu,
            TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.In),
            {Position = UDim2.new(-1, 0, 0,0)}
        ):Play()
        tabmenutoggled = true
    end)

    function TeamSkeetLib:LoadCfg(cfg)
        local content = http:JSONDecode(readfile(cfg))
        table.foreach(content, function(a,b)
        if TeamSkeetLib.Flags[a] then
            spawn(function()
              TeamSkeetLib.Flags[a]:Set(b)
           end)
        else
            warn("Error ", a,b)
        end
    end)
    end
    
    function TeamSkeetLib:SaveCfg(name)
        local content = {}
        for i,v in pairs(TeamSkeetLib.Flags) do
            content[i] = v.Value
        end
        writefile(name, tostring(http:JSONEncode(content))) -- FolderName.."/configs/"..name..".cfg"
    end
  
    function TeamSkeetLib:CreateCfg(name)
        local content = {}
        for i,v in pairs(TeamSkeetLib.Flags) do
            content[i] = v.Value
        end
        writefile(Folder.."/configs/"..name..".json", tostring(http:JSONEncode(content))) -- FolderName.."/configs/"..name..".cfg"
    end

    spawn(function()
        while wait() do
            MainFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
            TopFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TopBar
            HubTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
            -- TabTextUser.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TabTextBack
            -- TabTextUser.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
            Menu.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Menu
            SettingsFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
            RegisterFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
            UserFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
            MenuSelectFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
            ChangeImageFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].MainFrame
        end
    end)

    function TeamSkeetLib:Prompt(options)
        local Title = options.Title
        local Message = options.Message
        local Yes = options.Yes or function() return end
        local No = options.No or function() return end

        local PromptFrame = Instance.new("Frame")
        local PromptFrameCorner = Instance.new("UICorner")
        local PromptTitle = Instance.new("TextLabel")
        local YesButton = Instance.new("TextButton")
        local YesButtonCorner = Instance.new("UICorner")
        local NoButton = Instance.new("TextButton")
        local NoButtonCorner = Instance.new("UICorner")
        local PromptDesc = Instance.new("TextLabel")
        
        PromptFrame.Name = "PromptFrame"
        PromptFrame.Parent = MainFrame
        PromptFrame.BackgroundColor3 = Color3.fromRGB(22, 22, 22)
        PromptFrame.Position = UDim2.new(0.214156076, 0, 0.320843071, 0)
        PromptFrame.Size = UDim2.new(0, 366, 0, 191)
        
        PromptFrameCorner.Name = "PromptFrameCorner"
        PromptFrameCorner.Parent = PromptFrame
        
        PromptTitle.Name = "PromptTitle"
        PromptTitle.Parent = PromptFrame
        PromptTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        PromptTitle.BackgroundTransparency = 1.000
        PromptTitle.Position = UDim2.new(0.338797808, 0, 0.068062827, 0)
        PromptTitle.Size = UDim2.new(0, 118, 0, 25)
        PromptTitle.Font = Enum.Font.Gotham
        PromptTitle.Text = Title
        PromptTitle.TextColor3 = Color3.fromRGB(255, 255, 255)
        PromptTitle.TextSize = 14.000
        PromptTitle.RichText = true
        
        YesButton.Name = "YesButton"
        YesButton.Parent = PromptFrame
        YesButton.BackgroundColor3 = Color3.fromRGB(94, 151, 42)
        YesButton.Position = UDim2.new(0.0928961784, 0, 0.769633532, 0)
        YesButton.Size = UDim2.new(0, 137, 0, 31)
        YesButton.Font = Enum.Font.Gotham
        YesButton.TextColor3 = Color3.fromRGB(0, 0, 0)
        YesButton.TextSize = 15
        YesButton.TextColor3 = Color3.fromRGB(255, 255, 255)
        YesButton.Text = "Yes"
        
        YesButtonCorner.CornerRadius = UDim.new(0, 3)
        YesButtonCorner.Name = "YesButtonCorner"
        YesButtonCorner.Parent = YesButton
        
        NoButton.Name = "NoButton"
        NoButton.Parent = PromptFrame
        NoButton.BackgroundColor3 = Color3.fromRGB(190, 38, 38)
        NoButton.Position = UDim2.new(0.532786846, 0, 0.769633532, 0)
        NoButton.Size = UDim2.new(0, 137, 0, 31)
        NoButton.Font = Enum.Font.Gotham
        NoButton.TextColor3 = Color3.fromRGB(0, 0, 0)
        NoButton.TextSize = 15
        NoButton.TextColor3 = Color3.fromRGB(255, 255, 255)
        NoButton.Text = "No"
        
        MouseOver(YesButton, MF_littleman)
        MouseOver(NoButton, MF_littleman)
        
        NoButtonCorner.CornerRadius = UDim.new(0, 3)
        NoButtonCorner.Name = "NoButtonCorner"
        NoButtonCorner.Parent = NoButton
        
        PromptDesc.Name = "PromptDesc"
        PromptDesc.Parent = PromptFrame
        PromptDesc.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        PromptDesc.BackgroundTransparency = 1.000
        PromptDesc.Position = UDim2.new(0.356557369, 0, 0.371727735, 0)
        PromptDesc.Size = UDim2.new(0, 111, 0, 25)
        PromptDesc.Font = Enum.Font.Gotham
        PromptDesc.Text = Message -- "Join Team <font color=\"#5e972a\">Skeet</font> <font color=\"#5a6cb6\">Discord</font>"
        PromptDesc.TextColor3 = Color3.fromRGB(255, 255, 255)
        PromptDesc.TextSize = 14.000
        PromptDesc.RichText = true

        YesButton.MouseButton1Click:Connect(function()
            PromptFrame:Destroy()
            pcall(Yes)
        end)

        NoButton.MouseButton1Click:Connect(function()
            PromptFrame:Destroy()
            pcall(No)
        end)
    end

    local Tabs = {}
    function Tabs:Tab(Title)
        local Container = Instance.new("ScrollingFrame")
        local ContainerLayout = Instance.new("UIListLayout")
        local Tab = Instance.new("TextButton")
        local TabCorner = Instance.new("UICorner")
        local TabPadding = Instance.new("UIPadding")

        Container.Name = "Container"
        Container.Parent = ContainerHold
        Container.Active = true
        Container.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
        Container.BackgroundTransparency = 1.000
        Container.BorderSizePixel = 0
        Container.Position = UDim2.new(0, 0, 0.20608899, 0)
        Container.Size = UDim2.new(1, 0, 0.559718966, 100)
        Container.ScrollBarThickness = 3
        Container.Visible = false
        
        Tab.Name = "Tab"
        Tab.Parent = TabFrame
        Tab.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TabToggled
        Tab.Size = UDim2.new(0, 153, 0, 29)
        Tab.AutoButtonColor = false
        Tab.Font = Enum.Font.Gotham
        Tab.Text = Title
        Tab.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
        Tab.TextSize = 14.000
        Tab.TextXAlignment = Enum.TextXAlignment.Left
        Tab.BackgroundTransparency = 1.000
        Tab.TextTransparency = 0.4

        spawn(function()
            while wait() do
                Tab.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TabToggled
                Tab.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
            end
        end)
        
        TabCorner.CornerRadius = UDim.new(0, 5)
        TabCorner.Name = "TabCorner"
        TabCorner.Parent = Tab
        
        TabPadding.Name = "TabPadding"
        TabPadding.Parent = Tab
        TabPadding.PaddingLeft = UDim.new(0, 10)
        
        ContainerLayout.Name = "ContainerLayout"
        ContainerLayout.Parent = Container
        ContainerLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
        ContainerLayout.SortOrder = Enum.SortOrder.LayoutOrder

        if FirstTab == false then
            Container.Visible = true
            Tab.BackgroundTransparency = 0
            Tab.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
            Tab.TextTransparency = 0
            FirstTab = true
        end

        Tab.MouseButton1Click:Connect(function()
            function Transform(object, stat)
                state = stat
                if state == true then
                    TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad),{TextTransparency = 0.4}):Play()
                    TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad),{TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2}):Play()
                    TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad),{BackgroundTransparency = 1}):Play()
                else
                    TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad),{TextTransparency = 0}):Play()
                    TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad),{TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor}):Play()
                    TweenService:Create(object,TweenInfo.new(.3, Enum.EasingStyle.Quad),{BackgroundTransparency = 0}):Play()
                end
            end
            for _,v in pairs(TabFrame:GetChildren()) do
                if v:IsA("TextButton") then
                    Transform(v, true)
                end
            end
            Transform(Tab, false)
            for _,v in pairs(ContainerHold:GetChildren()) do
                if v:IsA("ScrollingFrame") then
                    v.Visible = false
                end
            end

            Container.Visible = true

            TweenService:Create(
                Menu,
                TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.In),
                {Position = UDim2.new(-1, 0, 0,0)}
            ):Play()
        end)

        local Sections = {}
        function Sections:Section(Text)
            local Section = Instance.new("Frame")
            local Section2 = Instance.new("Frame")
            local SectionLayout = Instance.new("UIListLayout")
            local SecTitleFrame = Instance.new("Frame")
            local SectionTitle = Instance.new("TextLabel")
            local DetectSectionButton = Instance.new("TextButton")

            Section.Name = "Section"
            Section.Parent = Container
            Section.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
            Section.BackgroundTransparency = 1.000
            Section.Position = UDim2.new(0.0116279069, 0, 0, 0)
            Section.Size = UDim2.new(0.980000019, 0, 0, 255)

            Section2.Name = "Section2"
            Section2.Parent = Section
            Section2.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
            Section2.BackgroundTransparency = 1.000
            Section2.BorderColor3 = Color3.fromRGB(27, 42, 53)
            Section2.Position = UDim2.new(0, 0, 0, 0)
            Section2.Size = UDim2.new(1, 0, 1, 0)

            DetectSectionButton.Name = "DetectSectionButton"
            DetectSectionButton.Parent = Section
            DetectSectionButton.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
            DetectSectionButton.BackgroundTransparency = 1.000
            DetectSectionButton.Size = UDim2.new(1, 0, 1, 0)
            DetectSectionButton.Font = Enum.Font.SourceSans
            DetectSectionButton.Text = ""
            DetectSectionButton.TextColor3 = Color3.fromRGB(0, 0, 0)
            DetectSectionButton.TextSize = 14.000
            DetectSectionButton.ZIndex = 0
            
            SectionLayout.Name = "SectionLayout"
            SectionLayout.Parent = Section2
            SectionLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
            SectionLayout.SortOrder = Enum.SortOrder.LayoutOrder
            SectionLayout.Padding = UDim.new(0, 9)

            -- DetectSectionButton.MouseEnter:Connect(function()
            --     TabTextUser.Text = Text
            -- end)
            
            SecTitleFrame.Name = "SecTitleFrame"
            SecTitleFrame.Parent = Section2
            SecTitleFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
            SecTitleFrame.BackgroundTransparency = 1.000
            SecTitleFrame.Position = UDim2.new(0.00896286033, 0, 0.0156862754, 0)
            SecTitleFrame.Size = UDim2.new(0, 538, 0, 30)
            
            SectionTitle.Name = "SectionTitle"
            SectionTitle.Parent = SecTitleFrame
            SectionTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
            SectionTitle.BackgroundTransparency = 1.000
            SectionTitle.Position = UDim2.new(0.0436458103, 0, 0.441176474, 0)
            SectionTitle.Size = UDim2.new(0, 500, 0, 10)
            SectionTitle.Font = Enum.Font.Gotham
            SectionTitle.Text = Text
            SectionTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
            SectionTitle.TextSize = 12.000
            SectionTitle.TextTransparency = 0.380
            SectionTitle.TextXAlignment = Enum.TextXAlignment.Left
            SectionTitle.TextYAlignment = Enum.TextYAlignment.Bottom

            spawn(function()
                while wait() do
                    SectionTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                end
            end)

            Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y + 10)

            local ContainerItems = {}
            function ContainerItems:Button(Text, callback)
                local Button = Instance.new("TextButton")
                local ButtonCorner = Instance.new("UICorner")
                local ButtonPadding = Instance.new("UIPadding")
                local ButtonTitle = Instance.new("TextLabel")

                Button.Name = "Button"
                Button.Parent = Section2
                Button.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Button
                Button.Position = UDim2.new(0.0518352352, 0, 0.117647059, 0)
                Button.Size = UDim2.new(0, 484, 0, 33)
                Button.AutoButtonColor = false
                Button.Font = Enum.Font.Gotham
                Button.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                Button.TextSize = 14.000
                Button.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
                Button.TextTransparency = 0.380
                Button.TextXAlignment = Enum.TextXAlignment.Right
                Button.ClipsDescendants = true
                MouseOver(Button, MF_littleman)
                
                ButtonCorner.CornerRadius = UDim.new(0, 4)
                ButtonCorner.Name = "ButtonCorner"
                ButtonCorner.Parent = Button
                
                ButtonPadding.Name = "ButtonPadding"
                ButtonPadding.Parent = Button
                ButtonPadding.PaddingRight = UDim.new(0, 10)
                
                ButtonTitle.Name = "ButtonTitle"
                ButtonTitle.Parent = Button
                ButtonTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                ButtonTitle.BackgroundTransparency = 1.000
                ButtonTitle.Position = UDim2.new(0.0189873409, 0, 0.303030312, 0)
                ButtonTitle.Size = UDim2.new(0, 51, 0, 13)
                ButtonTitle.Font = Enum.Font.Gotham
                ButtonTitle.Text = Text
                ButtonTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                ButtonTitle.TextSize = 14.000
                ButtonTitle.TextXAlignment = Enum.TextXAlignment.Left

                Button.MouseButton1Click:Connect(function()
                    pcall(callback)
                    Ripple(Button)
                end)
                spawn(function()
                    while wait() do
                        Button.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Button
                        Button.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                        ButtonTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                    end
                end)
                Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y  + 10)
                Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)
            end

            function ContainerItems:Toggle(Text, callback)
                local Togglefunc, ToggleToggled = {Value = false}, false

                local Toggle = Instance.new("TextButton")
                local ToggleCorner = Instance.new("UICorner")
                local ToggleTitle = Instance.new("TextLabel")
                local ToggleFrame = Instance.new("Frame")
                local ToggleFrameCorner = Instance.new("UICorner")
                local ToggleToggleFrame = Instance.new("Frame")
                local ToggleFrameCorner_2 = Instance.new("UICorner")
                local ToggleIcon = Instance.new("ImageLabel")

                Toggle.Name = "Toggle"
                Toggle.Parent = Section2
                Toggle.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Toggle
                Toggle.Position = UDim2.new(0.0518352352, 0, 0.117647059, 0)
                Toggle.Size = UDim2.new(0, 484, 0, 33)
                Toggle.AutoButtonColor = false
                Toggle.Font = Enum.Font.Gotham
                Toggle.Text = ""
                Toggle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                Toggle.TextSize = 14.000
                Toggle.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
                Toggle.TextTransparency = 0.380
                Toggle.TextXAlignment = Enum.TextXAlignment.Right
                MouseOver(Toggle, MF_littleman)
                
                ToggleCorner.CornerRadius = UDim.new(0, 4)
                ToggleCorner.Name = "ToggleCorner"
                ToggleCorner.Parent = Toggle
                
                ToggleTitle.Name = "ToggleTitle"
                ToggleTitle.Parent = Toggle
                ToggleTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                ToggleTitle.BackgroundTransparency = 1.000
                ToggleTitle.Position = UDim2.new(0.0192924645, 0, 0.303030312, 0)
                ToggleTitle.Size = UDim2.new(0, 51, 0, 13)
                ToggleTitle.Font = Enum.Font.Gotham
                ToggleTitle.Text = Text
                ToggleTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                ToggleTitle.TextSize = 14.000
                ToggleTitle.TextXAlignment = Enum.TextXAlignment.Left
                
                ToggleFrame.Name = "ToggleFrame"
                ToggleFrame.Parent = Toggle
                ToggleFrame.BackgroundColor3 = Color3.fromRGB(55, 55, 55)
                ToggleFrame.Position = UDim2.new(0.952479362, -10, 0.151515156, 0)
                ToggleFrame.Size = UDim2.new(0, 22, 0, 22)
                ToggleFrame.ZIndex = 2
                
                ToggleFrameCorner.CornerRadius = UDim.new(0, 3)
                ToggleFrameCorner.Name = "ToggleFrameCorner"
                ToggleFrameCorner.Parent = ToggleFrame
                
                ToggleToggleFrame.Name = "ToggleToggleFrame"
                ToggleToggleFrame.Parent = ToggleFrame
                ToggleToggleFrame.BackgroundColor3 = Color3.fromRGB(30, 30, 30)
                ToggleToggleFrame.Position = UDim2.new(0.0454545468, 0, 0.0454545468, 0)
                ToggleToggleFrame.Size = UDim2.new(1, -2, 1, -2)
                
                ToggleFrameCorner_2.CornerRadius = UDim.new(0, 3)
                ToggleFrameCorner_2.Name = "ToggleFrameCorner"
                ToggleFrameCorner_2.Parent = ToggleToggleFrame
                
                ToggleIcon.Name = "ToggleIcon"
                ToggleIcon.Parent = ToggleToggleFrame
                ToggleIcon.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                ToggleIcon.BackgroundTransparency = 1.000
                ToggleIcon.Position = UDim2.new(0.150000006, 0, 0.150000006, 0)
                ToggleIcon.Size = UDim2.new(1, -6, 1, -6)
                ToggleIcon.ZIndex = 5
                ToggleIcon.Image = "rbxassetid://6031094667"
                ToggleIcon.ImageTransparency = 1

                spawn(function()
                    while wait() do
                        Toggle.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Toggle
                        Toggle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                        ToggleTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                    end
                end)

                function Togglefunc:Set(val)
                    Togglefunc.Value = val
                    if Togglefunc.Value == false then
                        TweenService:Create(ToggleIcon,TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),{ImageTransparency= 1}):Play()
                        TweenService:Create(ToggleToggleFrame,TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),{BackgroundColor3= Color3.fromRGB(30, 30, 30)}):Play()

                    else
                        TweenService:Create(ToggleIcon,TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),{ImageTransparency= 0}):Play()
                        TweenService:Create(ToggleToggleFrame,TweenInfo.new(.2, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),{BackgroundColor3= TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].ToggleToggled}):Play()
                    end
                    return pcall(callback, Togglefunc.Value)
                end

                Toggle.MouseButton1Click:Connect(function()
                    Togglefunc.Value = not Togglefunc.Value
                    Togglefunc:Set(Togglefunc.Value)
                end)

                Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y  + 10)
                Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)

                TeamSkeetLib.Flags[Text] = Togglefunc
                return Togglefunc
            end
            function ContainerItems:Dropdown(Text, list, callback)
                local dropfunc, DropToggled = {Value = nil}, false
                local Dropdown = Instance.new("TextButton")
                local DropdownCorner = Instance.new("UICorner")
                local DropdownTitle = Instance.new("TextLabel")
                local Arrow = Instance.new("ImageLabel")
                local DropdownFrame = Instance.new("Frame")
                local DropdownFrameLayout = Instance.new("UIListLayout")

                Dropdown.Name = "Dropdown"
                Dropdown.Parent = Section2
                Dropdown.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
                Dropdown.Position = UDim2.new(0.0518352352, 0, 0.117647059, 0)
                Dropdown.Size = UDim2.new(0, 484, 0, 33)
                Dropdown.AutoButtonColor = false
                Dropdown.Font = Enum.Font.Gotham
                Dropdown.Text = ""
                Dropdown.TextColor3 = Color3.fromRGB(210, 210, 210)
                Dropdown.TextSize = 14.000
                Dropdown.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
                Dropdown.TextTransparency = 0.380
                Dropdown.TextXAlignment = Enum.TextXAlignment.Right
                MouseOver(Dropdown, MF_littleman)
                
                DropdownCorner.CornerRadius = UDim.new(0, 4)
                DropdownCorner.Name = "DropdownCorner"
                DropdownCorner.Parent = Dropdown
                
                DropdownTitle.Name = "DropdownTitle"
                DropdownTitle.Parent = Dropdown
                DropdownTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                DropdownTitle.BackgroundTransparency = 1.000
                DropdownTitle.Position = UDim2.new(0.0192924645, 0, 0.272727281, 0)
                DropdownTitle.Size = UDim2.new(0, 51, 0, 13)
                DropdownTitle.Font = Enum.Font.Gotham
                DropdownTitle.Text = Text
                DropdownTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                DropdownTitle.TextSize = 14.000
                DropdownTitle.TextXAlignment = Enum.TextXAlignment.Left

                spawn(function()
                    while wait() do
                        Dropdown.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Dropdown
                        DropdownTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                    end
                end)
                
                Arrow.Name = "Arrow"
                Arrow.Parent = Dropdown
                Arrow.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Arrow.BackgroundTransparency = 1.000
                Arrow.Position = UDim2.new(0.933884323, 0, 0.181818187, 0)
                Arrow.Size = UDim2.new(0, 20, 0, 20)
                Arrow.Image = "rbxassetid://6034818379"
                
                DropdownFrame.Name = "DropdownFrame"
                DropdownFrame.Parent = Section2
                DropdownFrame.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                DropdownFrame.BackgroundTransparency = 1.000
                DropdownFrame.Position = UDim2.new(0.0518352352, 0, 0.647058845, 0)
                DropdownFrame.Size = UDim2.new(0, 484, 0, 0)
                DropdownFrame.Visible = false
                
                DropdownFrameLayout.Name = "DropdownFrameLayout"
                DropdownFrameLayout.Parent = DropdownFrame
                DropdownFrameLayout.HorizontalAlignment = Enum.HorizontalAlignment.Center
                DropdownFrameLayout.SortOrder = Enum.SortOrder.LayoutOrder
                DropdownFrameLayout.Padding = UDim.new(0, 5)

                function dropfunc:Set(val)
                    dropfunc.Value = val
                    DropdownTitle.Text = Text .. " - " .. val
                    return pcall(callback, dropfunc.Value)
                end

                Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y  + 10)
                Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)

                Dropdown.MouseButton1Click:Connect(function()
                    if DropToggled == false then
                        TweenService:Create(
                            Arrow,
                            TweenInfo.new(.3, Enum.EasingStyle.Quad),
                            {Rotation = 180}
                        ):Play()
                        DropdownFrame.Visible = true
                        TweenService:Create(
                            DropdownFrame,
                            TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                            {Size = UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)}
                        ):Play()
                        repeat task.wait()
                            Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y  + 10)
                            Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)
                        until DropdownFrame.Size == UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)
                    else
                        TweenService:Create(
                            Arrow,
                            TweenInfo.new(.3, Enum.EasingStyle.Quad),
                            {Rotation = 0}
                        ):Play()
                        TweenService:Create(
                            DropdownFrame,
                            TweenInfo.new(.1, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                            {Size = UDim2.new(0, 484, 0, 0)}
                        ):Play()
                        repeat wait()
                            Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)
                            Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y)
                        until DropdownFrame.Size == UDim2.new(0, 484, 0, 0)
                        Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y)
                        DropdownFrame.Visible = false
                    end
                    DropToggled = not DropToggled
                end)

                for _,v in pairs(list) do
                    local DropItem = Instance.new("TextButton")
                    local DropItemCorner = Instance.new("UICorner")
                    local DropItemPadding = Instance.new("UIPadding")

                    DropItem.Name = "DropItem"
                    DropItem.Parent = DropdownFrame
                    DropItem.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].DropdownItem
                    DropItem.Position = UDim2.new(-0.0447763503, 0, 0, 0)
                    DropItem.Size = UDim2.new(0, 484, 0, 22)
                    DropItem.AutoButtonColor = false
                    DropItem.Font = Enum.Font.Gotham
                    DropItem.Text = v
                    DropItem.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                    DropItem.TextSize = 14.000
                    DropItem.TextTransparency = 0.400
                    DropItem.TextXAlignment = Enum.TextXAlignment.Left

                    spawn(function()
                        while wait() do
                            DropItem.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].DropdownItem
                            DropItem.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                        end
                    end)
                    
                    DropItemCorner.CornerRadius = UDim.new(0, 3)
                    DropItemCorner.Name = "DropItemCorner"
                    DropItemCorner.Parent = DropItem
                    
                    DropItemPadding.Name = "DropItemPadding"
                    DropItemPadding.Parent = DropItem
                    DropItemPadding.PaddingLeft = UDim.new(0, 7)
                    DropdownFrame.Size = UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)

                    DropItem.MouseButton1Click:Connect(function()
                        dropfunc:Set(v)
                        TweenService:Create(
                            Arrow,
                            TweenInfo.new(.3, Enum.EasingStyle.Quad),
                            {Rotation = 0}
                        ):Play()
                        TweenService:Create(
                            DropdownFrame,
                            TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                            {Size = UDim2.new(0, 484, 0, 0)}
                        ):Play()
                        repeat wait()
                            Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)
                            Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y)
                        until DropdownFrame.Size == UDim2.new(0, 484, 0, 0)
                        Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y)
                        DropdownFrame.Visible = false
                        DropToggled = false
                    end)
                end

                function dropfunc:Refresh(newlist)
                    for _,v in pairs(DropdownFrame:GetChildren()) do
                        if v:IsA("TextButton") then
                            v:Destroy()
                        end
                    end

                    for _,v in pairs(newlist) do
                        local DropItem = Instance.new("TextButton")
                        local DropItemCorner = Instance.new("UICorner")
                        local DropItemPadding = Instance.new("UIPadding")
    
                        DropItem.Name = "DropItem"
                        DropItem.Parent = DropdownFrame
                        DropItem.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].DropdownItem
                        DropItem.Position = UDim2.new(-0.0447763503, 0, 0, 0)
                        DropItem.Size = UDim2.new(0, 484, 0, 22)
                        DropItem.AutoButtonColor = false
                        DropItem.Font = Enum.Font.Gotham
                        DropItem.Text = v
                        DropItem.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                        DropItem.TextSize = 14.000
                        DropItem.TextTransparency = 0.400
                        DropItem.TextXAlignment = Enum.TextXAlignment.Left

                        spawn(function()
                            while wait() do
                                DropItem.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].DropdownItem
                                DropItem.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                            end
                        end)
                        
                        DropItemCorner.CornerRadius = UDim.new(0, 3)
                        DropItemCorner.Name = "DropItemCorner"
                        DropItemCorner.Parent = DropItem
                        
                        DropItemPadding.Name = "DropItemPadding"
                        DropItemPadding.Parent = DropItem
                        DropItemPadding.PaddingLeft = UDim.new(0, 7)
                        DropdownFrame.Size = UDim2.new(0, 484, 0, DropdownFrameLayout.AbsoluteContentSize.Y + 10)
    
                        DropItem.MouseButton1Click:Connect(function()
                            dropfunc:Set(v)
                            TweenService:Create(
                                Arrow,
                                TweenInfo.new(.3, Enum.EasingStyle.Quad),
                                {Rotation = 0}
                            ):Play()
                            TweenService:Create(
                                DropdownFrame,
                                TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),
                                {Size = UDim2.new(0, 484, 0, 0)}
                            ):Play()
                            repeat wait()
                                Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)
                                Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y)
                            until DropdownFrame.Size == UDim2.new(0, 484, 0, 0)
                            Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y)
                            DropdownFrame.Visible = false
                            DropToggled = false
                        end)
                    end
                end


                TeamSkeetLib.Flags[Text] = dropfunc
                return dropfunc
            end

            function ContainerItems:Slider(Text, min, max, start, callback)
                local sliderfunc, dragging = {Value = start}, false

                local Slider = Instance.new("TextButton")
                local SliderCorner = Instance.new("UICorner")
                local SliderPadding = Instance.new("UIPadding")
                local SliderTitle = Instance.new("TextLabel")
                local Value = Instance.new("TextBox")
                local SliderFrame = Instance.new("Frame")
                local SliderFrameCorner = Instance.new("UICorner")
                local SliderFrame_2 = Instance.new("Frame")
                local SliderFrameCorner_2 = Instance.new("UICorner")

                Slider.Name = "Slider"
                Slider.Parent = Section2
                Slider.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Slider
                Slider.Position = UDim2.new(0.0518352352, 0, 0.647058845, 0)
                Slider.Size = UDim2.new(0, 484, 0, 44)
                Slider.AutoButtonColor = false
                Slider.Font = Enum.Font.Gotham
                Slider.Text = ""
                Slider.TextColor3 = Color3.fromRGB(210, 210, 210)
                Slider.TextSize = 14.000
                Slider.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
                Slider.TextTransparency = 0.380
                Slider.TextXAlignment = Enum.TextXAlignment.Right
                MouseOver(Slider, MF_littleman)
                
                SliderCorner.CornerRadius = UDim.new(0, 4)
                SliderCorner.Name = "SliderCorner"
                SliderCorner.Parent = Slider
                
                SliderPadding.Name = "SliderPadding"
                SliderPadding.Parent = Slider
                SliderPadding.PaddingRight = UDim.new(0, 10)
                
                SliderTitle.Name = "SliderTitle"
                SliderTitle.Parent = Slider
                SliderTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                SliderTitle.BackgroundTransparency = 1.000
                SliderTitle.Position = UDim2.new(0.0189873409, 0, 0.121212125, 0)
                SliderTitle.Size = UDim2.new(0, 51, 0, 13)
                SliderTitle.Font = Enum.Font.Gotham
                SliderTitle.Text = Text
                SliderTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                SliderTitle.TextSize = 14.000
                SliderTitle.TextXAlignment = Enum.TextXAlignment.Left
                
                Value.Name = "Value"
                Value.Parent = Slider
                Value.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                Value.BackgroundTransparency = 1.000
                Value.Position = UDim2.new(0.959915638, 0, 0.121212125, 0)
                Value.Size = UDim2.new(0, 21, 0, 13)
                Value.Font = Enum.Font.Gotham
                Value.Text = tostring(start and math.floor((start / max) * (max - min) + min) or 0)
                Value.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                Value.TextSize = 14.000
                Value.TextTransparency = 0.400
                Value.TextXAlignment = Enum.TextXAlignment.Right
                
                SliderFrame.Name = "SliderFrame"
                SliderFrame.Parent = Slider
                SliderFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].SliderBar
                SliderFrame.BorderSizePixel = 0
                SliderFrame.Position = UDim2.new(0, 19, 0, 31)
                SliderFrame.Size = UDim2.new(1, -20, 0, 5)
                
                SliderFrameCorner.Name = "SliderFrameCorner"
                SliderFrameCorner.Parent = SliderFrame
                
                SliderFrame_2.Name = "SliderFrame"
                SliderFrame_2.Parent = SliderFrame
                SliderFrame_2.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].SliderInc
                SliderFrame_2.BorderSizePixel = 0
                SliderFrame_2.Size = UDim2.new((start or 0) / max, 0, 0, 5)
                
                SliderFrameCorner_2.Name = "SliderFrameCorner"
                SliderFrameCorner_2.Parent = SliderFrame_2

                Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y  + 70)
                Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 30)

                spawn(function()
                    while wait() do
                        Slider.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Slider
                        SliderTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                        Value.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                        SliderFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].SliderBar
                        SliderFrame_2.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].SliderInc
                    end
                end)

                function sliderfunc:Set(val)
                    sliderfunc.Value = val
                        local a = tostring(val and math.floor((val / max) * (max - min) + min) or 0)
                        Value.Text = tostring(a)
                        TweenService:Create(SliderFrame_2,TweenInfo.new(.3, Enum.EasingStyle.Quad, Enum.EasingDirection.Out),{Size = UDim2.new((val or 0) / max, 0, 0, 5)}):Play()
                        sliderfunc.Value = val
                    return pcall(callback, val)
                end

                local function slide(input)
                    local pos =
                       UDim2.new(
                          math.clamp((input.Position.X - SliderFrame.AbsolutePosition.X) / SliderFrame.AbsoluteSize.X, 0, 1),
                          0,
                          0,
                          5
                       )
                       SliderFrame_2:TweenSize(pos, Enum.EasingDirection.Out, Enum.EasingStyle.Quart, 0.3, true)
                    local val = math.floor(((pos.X.Scale * max) / max) * (max - min) + min)
                    Value.Text = tostring(val)
                    pcall(callback, val)
                 end
          
                 SliderFrame.InputBegan:Connect(
                    function(input)
                       if input.UserInputType == Enum.UserInputType.MouseButton1 then
                          slide(input)
                          dragging = true
                       end
                    end
                 )
          
                 SliderFrame.InputEnded:Connect(
                    function(input)
                       if input.UserInputType == Enum.UserInputType.MouseButton1 then
                          dragging = false
                       end
                    end
                 )
          
                 UserInputService.InputChanged:Connect(
                    function(input)
                       if dragging and input.UserInputType == Enum.UserInputType.MouseMovement then
                          slide(input)
                       end
                    end)

                    Value.FocusLost:Connect(function(ep)
                        if max < tonumber(Value.Text) then
                            sliderfunc:Set(max)
                        else
                            sliderfunc:Set(Value.Text)
                        end
                     end)
        
                 sliderfunc:Set(Value.Text)
                 TeamSkeetLib.Flags[Text] = sliderfunc
                 return sliderfunc
            end

            function ContainerItems:Bind(text, presset, callback)
                local bindfunc, key, Toggled = {Value = ""}, presset.Name, false

                local Bind = Instance.new("TextButton")
                local BindCorner = Instance.new("UICorner")
                local BindTitle = Instance.new("TextLabel")
                local BindFrame = Instance.new("Frame")
                local BindFrameCorner = Instance.new("UICorner")
                local BindText = Instance.new("TextLabel")

                Bind.Name = "Bind"
                Bind.Parent = Section2
                Bind.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Bind
                Bind.Position = UDim2.new(0.0518352352, 0, 0.117647059, 0)
                Bind.Size = UDim2.new(0, 484, 0, 33)
                Bind.AutoButtonColor = false
                Bind.Font = Enum.Font.Gotham
                Bind.Text = ""
                Bind.TextColor3 = Color3.fromRGB(210, 210, 210)
                Bind.TextSize = 14.000
                Bind.TextStrokeColor3 = Color3.fromRGB(210, 210, 210)
                Bind.TextTransparency = 0.380
                Bind.TextXAlignment = Enum.TextXAlignment.Right
                MouseOver(Bind, MF_littleman)
                
                BindCorner.CornerRadius = UDim.new(0, 4)
                BindCorner.Name = "BindCorner"
                BindCorner.Parent = Bind
                
                BindTitle.Name = "BindTitle"
                BindTitle.Parent = Bind
                BindTitle.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                BindTitle.BackgroundTransparency = 1.000
                BindTitle.Position = UDim2.new(0.0189873409, 0, 0.303030312, 0)
                BindTitle.Size = UDim2.new(0, 51, 0, 13)
                BindTitle.Font = Enum.Font.Gotham
                BindTitle.Text = text
                BindTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                BindTitle.TextSize = 14.000
                BindTitle.TextXAlignment = Enum.TextXAlignment.Left
                
                BindFrame.Name = "BindFrame"
                BindFrame.Parent = Bind
                BindFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].BindSec
                BindFrame.Position = UDim2.new(0.752066135, 0, 0.0909090936, 0)
                BindFrame.Size = UDim2.new(0, 108, 0, 27)
                
                BindFrameCorner.CornerRadius = UDim.new(0, 3)
                BindFrameCorner.Name = "BindFrameCorner"
                BindFrameCorner.Parent = BindFrame
                
                BindText.Name = "BindText"
                BindText.Parent = BindFrame
                BindText.BackgroundColor3 = Color3.fromRGB(255, 255, 255)
                BindText.BackgroundTransparency = 1.000
                BindText.Position = UDim2.new(0.291041046, 0, 0.185185194, 0)
                BindText.Size = UDim2.new(0, 45, 0, 17)
                BindText.Font = Enum.Font.Gotham
                BindText.Text = key
                BindText.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                BindText.TextSize = 14.000
                BindText.TextTransparency = 0.400

                spawn(function()
                    while wait() do
                        Bind.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].Bind
                        BindTitle.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor
                        BindText.TextColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].TextColor2
                        BindFrame.BackgroundColor3 = TeamSkeetLib.Themes[TeamSkeetLib.Settings.Theme].BindSec
                    end
                end)
            
                Bind.MouseButton1Click:Connect(function()
                    BindText.Text = "..."
                    local inputwait = game:GetService("UserInputService").InputBegan:wait()
                    if inputwait.KeyCode.Name ~= "Unknown" then
                        BindText.Text = inputwait.KeyCode.Name
                        key = inputwait.KeyCode.Name
                        return
                    end
                end)
            
                UserInputService.InputBegan:Connect(function(input, pressed)
                    if input.KeyCode == Enum.KeyCode[BindText.Text] then
                        Toggled = not Toggled
                        pcall(callback, Toggled)
                    end
                end)


                function bindfunc:Set(val)
                    bindfunc.Value = val.Name
                    BindText.Text = val.Name
                end

                Container.CanvasSize = UDim2.new(0, 0, 0, ContainerLayout.AbsoluteContentSize.Y  + 10)
                Section.Size = UDim2.new(0.980000019, 0, 0, SectionLayout.AbsoluteContentSize.Y + 10)

                TeamSkeetLib.Flags[text] = bindfunc
                return bindfunc
                end
            return ContainerItems
        end
        return Sections
    end
    return Tabs
end

return TeamSkeetLib
